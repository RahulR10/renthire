<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'App\Http\Controllers\Api', 'as' => 'api.'],function(){
    Route::apiresources([
        'make'  => 'MakeController',
        'model'  => 'VehicleModelController',
        // 'user'   => 'UserController'
    ]);
    Route::apiresource('user', 'UserController')->only('store');

    Route::post('login', 'UserController@login');

    Route::group(['middleware' => 'auth:api'], function () {
        // Route::apiresources([
        //     // 'user'   => 'UserController'
        // ]);
        // Route::apiresource('user', 'UserController')->except('store');
        Route::get('profile', 'UserController@show')->name('user.show');
        Route::get('vehicle-type', 'VehicleTypeController@index')->name('vehicletype.show');
        Route::apiresource('rent', 'RentController')->only(['index','store']);
        Route::apiresource('hire', 'HireController')->only(['index','store']);
        Route::apiresource('dpr', 'DprController')->only(['index','store']);
    });
});