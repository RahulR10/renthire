<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/migrate', function () {
    Artisan::call('migrate');
    return 'Migrated';
});
Route::get('/clear-cache', function () {
    Artisan::call('optimize:clear');
    return 'Cleared';
});

Route::get('/link-storage', function () {
    Artisan::call('storage:link');
    return 'Storage';
});

Route::get('/seeder', function () {
    Artisan::call('db:seed');
    return 'seeder';
});

Route::group(['namespace' => 'App\Http\Controllers'],function(){

    #Admin Routes
    Route::get('/login', 'AuthController@showLogin')->name('login');
    Route::group(['prefix'=>'admin-panel','as'=>'admin.'], function()
    {
        Route::post('hire_form','HireFormController@store')->name('hireForm');
        Route::post('/login', 'AuthController@login')->name('login');
        Route::get('/changepassword', 'AuthController@showChangepassword')->name('changepassword');
        Route::get('/logout', 'AuthController@logout')->name('logout');

        Route::group(['middleware' => 'auth:admin'], function()
        {
            Route::get('/','DashboardController@dashboard')->name('dashboard');
            
            Route::get('wallet-status/{wallet}/{status}','WalletController@status')->name('wallet.status');

            Route::resources([
                'vehicle_type' => 'VehicleTypeController',
                'make' => 'MakeController',
                'vehicle_model' => 'VehicleModelController',
                'rent' => 'RentController',
                'hire' => 'HireController',
                'admin' => 'AdminController',
                'user' =>'UserController',
                'dpr' =>'DprController',
                'slider' => 'SliderController',
                'testimonial' => 'TestimonialController',
                'page' =>'PageController',
                'site_setting' => 'SiteSettingController',
                'wallet'=>'WalletController',
                // 'hire_form'=>'HireFormController',
            ]);
        });
    });

    #Vendor Routes
    Route::group(['prefix'=>'vendor-panel','as'=>'vendor.','namespace'=>'Vendor'], function()
    {
        Route::get('/login', 'AuthController@showLogin')->name('login');
        Route::post('/login', 'AuthController@login')->name('login.post');
        Route::get('/changepassword', 'AuthController@showChangepassword')->name('changepassword');
        Route::get('/logout', 'AuthController@logout')->name('logout');

        Route::group(['middleware' => 'auth'], function()
        {
            Route::get('/','DashboardController@dashboard')->name('dashboard');
            Route::get('company_edit/{user}','UserController@company_edit')->name('company_edit');
            Route::get('bank_edit/{user}','UserController@bank_edit')->name('bank_edit');
            Route::resources([
                'rent'  => 'RentController',
                'hire'  => 'HireController',
                'admin' => 'AdminController',
                'user'  =>'UserController',
                'dpr'   =>'DprController',
                'wallet'=>'WalletController',
            ]);
        });
    });

    
    #FrontEnd Routes
    Route::group(['namespace' => 'Web'], function() {
        Route::get('/','HomeController@index')->name('home');
        Route::get('Contact','ContactController@contact')->name('Contact');
        Route::get('Vendor/signin','VendorSignController@vendorsign')->name('Vendor.signin');
        Route::post('Vendor/store','VendorSignController@store')->name('Vendor.store');
        Route::get('rentalProduct','ProductController@rentalProduct')->name('rentalProduct');
        
        Route::group(['middleware' => 'vendor'], function(){
            Route::get('hire/{rent}','HireController@hire')->name('hire');
            Route::post('hire','HireController@store')->name('hire.store');
        });
        
        Route::get('{page:slug}','PageController@show')->name('page.show');
    });


});