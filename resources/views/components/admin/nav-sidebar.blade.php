<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
        <div class="sb-sidenav-menu">
            <div class="nav">
                <div class="sb-sidenav-menu-heading">Core</div>
                <a class="nav-link" href="{{ route('admin.dashboard') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                    Dashboard
                </a>

                <div class="sb-sidenav-menu-heading">Master</div>

                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsevehicle" aria-expanded="false" aria-controls="collapsePages">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                    Vehicle Type
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="collapsevehicle" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="{{route('admin.vehicle_type.create')}}">Add</a>
                        <a class="nav-link" href="{{route('admin.vehicle_type.index')}}">View</a>
                    </nav>
                </div>

                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsemake" aria-expanded="false" aria-controls="collapsePages">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                    Make
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="collapsemake" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="{{route('admin.make.create')}}">Add</a>
                        <a class="nav-link" href="{{route('admin.make.index')}}">View</a>
                    </nav>
                </div>
                
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsemodel" aria-expanded="false" aria-controls="collapsePages">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                    Model
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>

                <div class="collapse" id="collapsemodel" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="{{route('admin.vehicle_model.create')}}">Add</a>
                        <a class="nav-link" href="{{route('admin.vehicle_model.index')}}">View</a>
                    </nav>
                </div>

                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseslider" aria-expanded="false" aria-controls="collapsePages">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                    Slider
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                
                <div class="collapse" id="collapseslider" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="{{route('admin.slider.create')}}">Add</a>
                        <a class="nav-link" href="{{route('admin.slider.index')}}">View</a>
                    </nav>
                </div>

                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsetestimonial" aria-expanded="false" aria-controls="collapsePages">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                    Testimonial
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                
                <div class="collapse" id="collapsetestimonial" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="{{route('admin.testimonial.create')}}">Add</a>
                        <a class="nav-link" href="{{route('admin.testimonial.index')}}">View</a>
                    </nav>
                </div>

                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsepage" aria-expanded="false" aria-controls="collapsePages">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                    Page
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                
                <div class="collapse" id="collapsepage" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        @foreach($pages as $slug => $title)
                        <a class="nav-link" href="{{route('admin.page.edit', $slug)}}">{{  $title  }}</a>
                        @endforeach
                    </nav>
                </div>

                <div class="sb-sidenav-menu-heading">App</div>

                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapserent" aria-expanded="false" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                    Rent
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="collapserent" aria-labelledby="headingTwo" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="{{route('admin.rent.create')}}">Add</a>
                        <a class="nav-link" href="{{route('admin.rent.index')}}">View</a>
                    </nav>
                </div>

                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsehire" aria-expanded="false" aria-controls="collapsePages">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                    Hire
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="collapsehire" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="{{route('admin.hire.create')}}">Add</a>
                        <a class="nav-link" href="{{route('admin.hire.index')}}">View</a>
                    </nav>
                </div>

                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsewallet" aria-expanded="false" aria-controls="collapsePages">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                    Wallet
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="collapsewallet" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                <nav class="sb-sidenav-menu-nested nav">
                        {{-- <a class="nav-link" href="{{route('vendor.wallet.create')}}">Add</a> --}}
                        <a class="nav-link" href="{{route('admin.wallet.index')}}">View</a>
                    </nav>
                </div>

                <div class="sb-sidenav-menu-heading">User</div>   
                
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsestaff" aria-expanded="false" aria-controls="collapsePages">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                    Staff
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="collapsestaff" aria-labelledby="headingThree" data-parent="#sidenavAccordion">
                <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="{{route('admin.admin.create')}}">Add</a>
                        <a class="nav-link" href="{{route('admin.admin.index')}}">View</a>
                    </nav>
                </div>


                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsecustomer" aria-expanded="false" aria-controls="collapsePages">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                    Customer
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="collapsecustomer" aria-labelledby="headingThree" data-parent="#sidenavAccordion">
                <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="{{route('admin.user.create')}}">Add</a>
                        <a class="nav-link" href="{{route('admin.user.index')}}">view</a>
                    </nav>
                </div>

            </div>
            <a href="https://projects.rudrakshatech.com/rentandhire/public/migrate">
                <button type="button" class="btn btn-secondary m-2">Migrate</button>
            </a>
            <a href="https://projects.rudrakshatech.com/rentandhire/public/link-storage">
                <button type="button" class="btn btn-secondary m-2">Storage Link</button>
            </a>
        </div>


        <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
            <div class="sb-sidenav-footer small-name" style="color: white;">
                <div class="small ">Logged in as:</div>
            </div>
        </form>
    </nav>
</div>