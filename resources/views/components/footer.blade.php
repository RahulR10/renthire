<section class="footer">
    <div class="container">
      <div class="footer-middle">
        <div class="row">
          <div class="col-lg-5">
            <div class="footer-logo">
              <a href="{{ route('home') }}" class="footer-logo-link">
                <img src="{{ url("storage/".$siteSetting->footer_logo) }}" alt="">
              </a>
            </div>
            <div class="footer-content">
              {!! $page[0]->excerpt !!}
            </div>
            <div class="social-icons">
              @if (!empty($siteSetting->social_links['instagram']))
                <div class="insta">
                  <a href="{{ $siteSetting->social_links['instagram'] }}">
                    <i class="fa-brands fa-instagram"></i>
                  </a>
                </div>
              @endif
              @if (!empty($siteSetting->social_links['whatsapp']))
                <div class="whatsapp px-4">
                  <a href="{{ $siteSetting->social_links['whatsapp'] }}">
                    <i class="fa-brands fa-whatsapp"></i>
                  </a>
                </div>
              @endif
              @if (!empty($siteSetting->social_links['facebook']))
                <div class="fb">
                  <a href="{{ $siteSetting->social_links['facebook'] }}">
                    <i class="fa-brands fa-facebook"></i>
                  </a>
                </div>
              @endif
            </div>
          </div>
          <div class="col-lg-3 useful-links">
            <div class="footer-inner-title">
              Useful Links
            </div>
            <ul class="inner-links">
                @foreach ($pages as $slug => $title)
                <li class="py-2"> <a href="{{ route('page.show', $slug) }}">{{  $title  }} </a></li>
                
                @endforeach
                <li class="py-2"> <a href="{{ route('Contact') }}"> Contact Us</a> </li>
                <li class="py-2"> <a href="{{ route('rentalProduct') }}"> Rental </a> </li>
              {{-- <li class="py-2"> <a href="{{ route('About') }}"> About Us</a></li>
              <li class="py-2"> <a href="{{ route('privacypolicy') }}">Privacy Policy  </a></li> --}}
            </ul>
          </div>
          <div class="col-lg-3">
            <div class="footer-inner-title">Get In Touch</div>
            <ul class="inner-links">
              <li class="contact-list-item d-flex py-2">
                <div class="contact-list-item-icon">
                  <i class="fa-solid fa-phone"></i>
                </div>
                <div class="contact-list-item-content">
                  <div class="content-label">
                    For Sales/Rental Support
                  </div>
                  <div class="content-contact">
                    <a href="tel:+91{{ $siteSetting->phone_2 }}" style="color: #ffffff;">{{ $siteSetting->phone_2 }}</a>
                  </div>
                </div>
              </li>
              <li class="contact-list-item d-flex py-2">
                <div class="contact-list-item-icon">
                    <i class="fa-solid fa-stopwatch"></i>
                </div>
                <div class="contact-list-item-content">
                  <div class="content-label">
                    Address
                  </div>
                  <div class="content-contact"> {{ $siteSetting->address }} </div>
                </div>
              </li>
              <li class="contact-list-item d-flex py-2">
                <div class="contact-list-item-icon">
                  <i class="fa-solid fa-envelope-open-text"></i>
                </div>
                <div class="contact-list-item-content">
                  <div class="content-label">
                    Send Us Email
                  </div>
                  <div class="content-contact">
                    <a href="mailto:{{ $siteSetting->email }}" style="color: #ffffff;">{{ $siteSetting->email }}</a>
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="footer-bottom">
        <div class="footer-bootom-right">
          <div class="design-dvlp">
            <span>Design & Develop by <a class="text-decoration-none text-warning" href="https://suncitytechno.com/">Suncity Techno Pvt. Ltd. </a></span>
          </div>
        </div>
        <div class="footer-bootom-left">
          <div class="cpyright">
            <span>© Copyright 2022 <strong class="text-warning">Rent and Hire</strong> </span>
          </div>
        </div>
      </div>
    </div>
  </section>