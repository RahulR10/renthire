<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
        <div class="sb-sidenav-menu">
            <div class="nav">
                <a href="{{ route('home') }}" class="btn text-white mt-2">
                  Back To website
                </a>
                <div class="sb-sidenav-menu-heading">Core</div>
                <a class="nav-link" href="{{ route('vendor.dashboard') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                    Profile
                </a>

                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapserent" aria-expanded="false" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                    Rent
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="collapserent" aria-labelledby="headingTwo" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="{{route('vendor.rent.create')}}">Add</a>
                        <a class="nav-link" href="{{route('vendor.rent.index')}}">View</a>
                    </nav>
                </div>

                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsehire" aria-expanded="false" aria-controls="collapsePages">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                    Hire
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="collapsehire" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                <nav class="sb-sidenav-menu-nested nav">
                        {{-- <a class="nav-link" href="{{route('vendor.hire.create')}}">Add</a> --}}
                        <a class="nav-link" href="{{route('vendor.hire.index')}}">View</a>
                    </nav>
                </div>

                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsewallet" aria-expanded="false" aria-controls="collapsePages">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                    Wallet
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="collapsewallet" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                <nav class="sb-sidenav-menu-nested nav">
                        {{-- <a class="nav-link" href="{{route('vendor.wallet.create')}}">Add</a> --}}
                        <a class="nav-link" href="{{route('vendor.wallet.index')}}">View</a>
                    </nav>
                </div>

            </div>
        </div>
    </nav>
</div>