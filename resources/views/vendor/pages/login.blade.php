@extends('vendor.layout.inner') 

@section('site_title','Vendor Login')

@section('main_content')

    <div id="layoutAuthentication" class="login-page">
        <div id="layoutAuthentication_content">
            <main>
                <div class="login-header">
                    <div class="container-fluid">
                        <div class="login-title">
                            <a href="">Rent &  Hire - Vendor Registration</a>
                        </div>
                    </div>
                </div>
                <div class="login-content d-flex align-items-center flex-wrap justify-content-center" style="background : url('http://renthire.in/assets/images/banner.jpg'); background-size: cover;height:520px;">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-5" style="top:-20px;">
                                <div class="card shadow-lg border-0 rounded-lg mt-5">
                                    <div class="card-header"><h3 class="text-center font-weight-light my-4">Vendor Login</h3></div>
                                    <div class="card-body">
                                       
                                        <div class="alert alert-danger" id="error" style="display: none;"></div>
    
                                        {{ Form::open(['url' => route('vendor.login'), 'method' => 'post', 'id' => 'login-form']) }}

                                        <div class="form-group" id="otpsend">
                                            <label for="small mb-1" for="phone">Phone</label>
                                            <input name="phone" type="text" id="phone" class="form-control" placeholder="99**99**99">
                                            <div id="recaptcha-container" class="mt-4"></div>
                                            <button type="button" class="btn btn-primary mt-3" onclick="sendOTP();">Send otp</button>
                                        </div>
                                            
                                        {{ Form::close() }}

                                        <div class="mb-5 mt-5" id="successShow" style="display: none;">
                                            <h5>Add Verifiation Code</h5>
                                            <div class="alert alert-success" id="successOtpAuth" style="display: none";></div>
                                            {{ Form::open(['url' => route('vendor.login'), 'method' => 'post']) }}
                                                <input type="text" id="verification" class="form-control" placeholder="verification-code">
                                                <button type="button" class="btn btn-danger mt-3" onclick="verify()">Verify Code</button>
                                            {{ Form::close() }}
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div id="layoutAuthentication_footer">
            <footer class="py-4 bg-light mt-auto">
                <div class="container-fluid">
                <div class="d-flex align-items-center justify-content-between small">

                    <div class="text-muted">
                        Design and Developed by 
                        <a href="http://www.rudrakshatech.com/">Rudrakh Tech Solution</a>
                    </div>

                    <div class="text-muted">Copyright &copy; Your Website 2020</div>

                </div>
                </div>
            </footer>
        </div>
    </div>
@endsection

<script src="https://www.gstatic.com/firebasejs/6.0.2/firebase.js"></script>

<script>
    var firebaseConfig = {
        apiKey: "AIzaSyCOkYHpO2PHvhECsMciocSc2PP9xLbrto0",
        authDomain: "sharmarahul-e22a2.firebaseapp.com",
        projectId: "sharmarahul-e22a2",
        storageBucket: "sharmarahul-e22a2.appspot.com",
        messagingSenderId: "333330930852",
        appId: "1:333330930852:web:db2234c78417f1eaf21a31",
        measurementId: "G-GPK6EPJ776"
    };
    firebase.initializeApp(firebaseConfig);
</script>
<script type="text/javascript">
    window.onload = function () {
        render();
    };
    function render() {
        window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
        recaptchaVerifier.render();
    }
    function sendOTP() {
        var number = '+91 ' + $("#phone").val();
        firebase.auth().signInWithPhoneNumber(number, window.recaptchaVerifier).then(function (confirmationResult) {
            window.confirmationResult = confirmationResult;
            coderesult = confirmationResult;
            console.log(coderesult);
            $("#successAuth").text("Message sent");
            $("#successAuth").show();
            $('#otpsend').hide();
            $('#successShow').show();
        }).catch(function (error) {
            $("#error").text(error.message);
            $("#error").show();
        });
    }
    function verify() {
        var code = $("#verification").val();
        coderesult.confirm(code).then(function (result) {
            var user = result.user;
            console.log(user);
            // $("#successOtpAuth").text("Auth is successful");
            // $("#successOtpAuth").show();

            $('#login-form').trigger('submit');
        }).catch(function (error) {
            $("#error").text(error.message);
            $("#error").show();
        });
    }
</script>