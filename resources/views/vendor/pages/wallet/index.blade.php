@extends('vendor.layout.inner')

@section('site_title','Wallet')

@section('content')
    <div class="container-fluid py-4">
        <div class="d-flex justify-content-between align-items-center">
            <div>
                <h2>Wallet</h2>
                <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item">
                        <a href="{{ route('vendor.dashboard') }}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">
                        Wallet
                    </li>
                </ol>
            </div>
            <div>
                <h4>
                    <span>Total Amount : {{ $total }}</span>
                </h4>
            </div>
        </div>
        
        <div class="d-flex justify-content-between align-items-center py-1">
            <div>
                {{ $wallets->firstItem() }} to {{ $wallets->lastItem() }} of {{ $wallets->total() }} are Showing
            </div>
            <div>
                <a href="{{ route('vendor.wallet.create')}}" class="btn btn-dark btn-sm float-end"><span class="py-4">withdraw</span></a>
            </div>
        </div>


        <table class="table table-striped py-4">
            <thead>
                <tr>
                    <th>Sr .No</th>
                    <th>User Name</th>
                    <th>Payment Type</th>
                    <th>Amount</th>
                    <th>Remarks</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($wallets as $index =>$wallet)
                <tr>    
                    <th>{{ $index + $wallets -> firstItem() }}</th>
                    <td>{{ $wallet -> user->name }}</td>
                    <td>{{ $wallet -> type }}</td>
                    <td>{{ $wallet -> amount }}</td>
                    <td>{{ $wallet -> remark }}</td>
                </tr>
                
                @endforeach
            </tbody>
        </table>
        {{ $wallets->links("pagination::bootstrap-5") }}
    </div>
@endsection