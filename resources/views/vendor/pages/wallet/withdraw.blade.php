@extends('vendor.layout.inner')

@section('site_title','withdraw')

@section('content')
    <div class="container-fluid py-4">
        <div class="d-flex justify-content-between align-items-center">
            <div>
                <h2>withdraw</h2>
                <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item">
                        <a href="{{ route('vendor.dashboard') }}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">
                        withdraw
                    </li>
                </ol>
            </div>
        </div>
        @include('backend.template.messages')
        {{ Form::open([
            'url' => route('vendor.wallet.store'),
            'files' => true,
            'method' => 'post'
        ]) }}
            <div class="row pt-5 bg-white">
                <div class="col-8  text-dark">
                    
                    <div class="mb-3">
                        {{ Form::label('amount','Amount') }}
                        {{  Form::number('amount',null, ['class' => 'form-control', 'placeholder' => 'Enter Amount','min'=>1 , 'max'=> $total  ])  }}
                        <span class="text-danger">
                            Note: Amount Not Greater than {{ $total }}
                        </span>
                    </div>
                        
                    <div class="mb-3">
                        {!! Form::label('remark', 'Remark') !!}
                        {{  Form::text('remark', null, ['class' => 'form-control', 'placeholder' => 'Enter Remark'])  }}
                    </div>
                </div>
            </div>
            <div class="pt-3">
                <button type="submit" class="btn btn-primary">Send</button>
        
                <button type="reset" class="btn btn-secondary">Reset</button>
            </div>
        {{ Form::close() }}
    </div>
@endsection

<script>

</script>