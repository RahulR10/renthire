@extends('vendor.layout.inner')

@section('site_title','Hire')

@section('content')

<div class="container-fluid">
    <div class="bgwhite">
        <h1>Edit Hire</h1>
        <h2>Dashboard/Hire/{{ $hire->title}}</h2>
    </div>

    {!! Form::open
        ([
            'url' => route('admin.hire.update',
            [$hire->id]),
            'method' => 'put'
        ]) 
    !!}
        @include('backend.pages.hire.form')

        <div class="pt-3">        
            <button type="submit" class="btn btn-primary">Update</button>

            <button type="reset" class="btn btn-secondary">Reset</button>
        </div>


    {!! Form::close() !!}    
</div>

@endsection