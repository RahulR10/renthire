@extends('vendor.layout.inner')

@section('site_title','Dashboard')

@section('content')

<div class="container-fluid py-4">
        <div class="bg-white">

            <h2>Add Hire</h2>
            
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.dashboard') }}">Dashboard</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.hire.index')}}">Hire</a>
                </li>
                <li class="breadcrumb-item active">Add Hire</li>
            </ol>

        </div>
        {!! Form::open
            ([
                'url' => route('admin.hire.store'), 
                'files' => true, 
                'method' => 'post'
            ]) 
        !!}
            @include('backend.pages.hire.form')
            <div class="pt-3">
                <button type="submit" class="btn btn-primary">Save</button>

                <button type="reset" class="btn btn-secondary">Reset</button>
            </div>

        {!! Form::close() !!}    
    </div>

@endsection