<div class="row pt-5 bg-white">
    <div class="col-8  text-dark">

        <input type="hidden" name="hire_id" value="{{ $hire->id }}">

        <div class="mb-3">
            {{ Form::label('diesel','Diesel') }}
            {{ Form::text('diesel',null,['class' => 'form-control','placeholder' => 'Diesel']) }}
        </div>

        <div class="mb-3">
            {!! Form::label('hour' , 'Hour')!!}
            {!! Form::text('hour', null,['class' => 'form-control' , 'placeholder' => 'Hour','required' => 'required'])!!}
        </div>
        
    
        <div class="mb-3">
            {{ Form::label('km','Killo Meter') }}
            {{ Form::text('km',null,['class' => 'form-control','placeholder'=>'Killo Meter']) }}
        </div>

    </div>
    <div class="col-sm-4">
        <div class="card mb-3">
            <div class="card-header">Signature</div>
            <div class="card-body text-center">
                <label class="image-container d-block mb-3">
                    <img src="{{ !empty($dpr->signature) ? url("storage/".$dpr->signature): url('vendor/image/plus.svg') }}" alt="image" id="faviconPreview" class="w-50">
                    <input type="file" name="signature" id="favicon" accept="image/" class="d-none file-choose" data-target="#faviconPreview">
                </label>
                <label for="favicon" class="btn btn-dark d-block">Upload Signature</label>
            </div>
        </div>
        
    </div>
</div>