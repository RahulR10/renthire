@extends('vendor.layout.inner')

@section('site_title','Daily Progress Report')

@section('content')

<div class="container-fluid py-4">
    <div class="bg-white">

        <h2>Add Daily Progress Report</h2>

        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item">
                <a href="{{ route('vendor.dashboard') }}">Dashboard</a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{ route('vendor.dpr.create')}}">Hire</a>
            </li>
            <li class="breadcrumb-item active">Add Hire</li>
        </ol>

    </div>

    @if($dprs->isEmpty())
        <div></div>
        @else
        <div>
            {{ $dprs->firstItem() }} to {{ $dprs->lastItem() }} of {{ $dprs->total() }} are Showing
        </div>

        <table class="table table-striped py-4">
            <thead>
                <tr>
                    <th>Sr No.</th>
                    <th>Diesel</th>
                    <th>Kilo Meter(km)</th>
                    <th>Hour(hr)</th>
                    <th>Status(Y/N)</th>
                    <th>More</th>
                </tr>
            </thead>
            <tbody>
                @foreach($dprs as $index => $dpr)
                <tr>
                    <th>{{ $index + $dprs -> firstItem() }}</th>
                    <td>{{$dpr -> diesel}}</td>
                    <td>{{$dpr -> km}}</td>
                    <td>{{$dpr -> hour}}</td>
                    <td>{{$dpr -> status}}</td>
                    <td>
                        <div class="dropdown">
                            <button class="btn btn-link" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            More
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <!-- <a class="dropdown-item" href="{{route('vendor.dpr.edit',[$dpr->id]) }}">Edit</a> -->
                                <!-- {{ Form::open(['url' => route('vendor.dpr.destroy', [$dpr->id]), 'method' => 'delete', 'class' => 'delete-form']) }} -->
                                <button type="button" class="delete-btn dropdown-item">Delete</button>
                                {{ Form::close() }}
                            
                            </div>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ $dprs->links("pagination::bootstrap-5") }}
    @endif
</div>

@endsection