@extends('backend.layout.inner') 

@section('main_content')

<div id="layoutAuthentication">
        <div id="layoutAuthentication_content">
            <main>
                <div class="container-fluid">
                    <div class="row justify-content-center">
                        <div class="col-lg-5 mt-5">
                            <div class="card shadow-lg border-0 rounded-lg mt-5  ">
                                <div class="card-header"><h3 class="text-center font-weight-light my-4 ">Change Password</h3></div>
                                <div class="card-body ">
                                    

                                    {{ Form::open(['url' => route('admin.changepassword'), 'method' => 'put']) }}
                                        
                                        <div class="form-group">
                                            <label class="small mb-1 " for="inputPassword">New Password</label>
                                            <input class="form-control py-4" id="inputPassword" type="password" name="password" placeholder="Enter password" />
                                        </div>

                                        <div class="form-group">
                                            <label class="small mb-1 " for="inputPassword">Confirm Password</label>
                                            <input class="form-control py-4" id="inputPassword" type="password" name="password" placeholder="Enter password" />
                                        </div>
                                        
                                        <div class="form-group d-flex align-items-center justify-content-between text-center mt-4 mb-0">
                                            
                                            <button type="submit" class="btn btn-primary mx-auto">Update</button>
                                        </div>
                                    {{ Form::close() }}
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div id="layoutAuthentication_footer">
            <footer class="py-4 bg-light mt-auto">
                <div class="container-fluid">
                <div class="d-flex align-items-center justify-content-between small">

                    <div class="text-muted">Design and Developed by <a href="http://www.rudrakshatech.com/">Rudrakh Tech Solution</a></div>

                    <div class="text-muted">Copyright &copy; Your Website 2020</div>

                </div>
                </div>
            </footer>
        </div>
    </div>
@endsection