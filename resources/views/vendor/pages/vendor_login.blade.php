<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Vendor - login | Rent & Hire</title>
    <link rel="stylesheet" href="{{ url('frontend/css/style.css') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="frontend/images/Logo.png">
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <!-- Fontawesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>
<body>

    <div class="login-page">
        <div class="login-header">
            <div class="container-fluid">
                <div class="login-title">
                    <a href="">Rent &  Hire - Vendor Registration</a>
                </div>
            </div>
        </div>
        <div class="login-content d-flex align-items-center flex-wrap justify-content-center" style="background : url('http://renthire.in/assets/images/banner.jpg'); background-size: cover">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-sm-6"></div>
                    <div class="col-sm-6">
                        <div class="login-vendor bg-white box-shadow">
                            <div class="login-vendor-title">
                                <h2 class="text-center text-danger">Vendor Login Panel</h2>
                                <div style="color: red;font-size: 15px;text-align: center !important;margin-top: 16px;"></div>
                            </div>
                            <form action="" method="post">
                                <div class="input-group custom mb-3">
                                    <input type="text" name="email" class="form-control form-control-lg" placeholder="Email Id">
                                </div>
                                <div class="input-group custom mb-3">
                                    <input type="password" name="password" class="form-control form-control-lg" placeholder="********">
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-input-checkbox" name="" id="">
                                            <label for="" class="custom-label">Remember Me</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="forgot-pass">
                                            <a href="">Forgot Password</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="input-group mb-0">
                                            <div class="vendor-login-button" type="submit">Login</div>
                                        </div>
                                        <div class="font-16 weight-600 pt-10 pb-10 text-center" data-color="#707373" style="color: rgb(112, 115, 115);">OR</div>
                                        <div class="input-group mb-0">
                                            {{-- <div class="vendor-register-button" type="submit">Register</div> --}}
                                            <a href="{{ route('Vendor.signin') }}" type="button" class="vendor-register-button text-decoration-none">Register</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
</body>
</html>