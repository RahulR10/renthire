@extends('vendor.layout.inner')

@section('site_title','User Setting')

@section('content')

    <div class="container-fluid py-4">
        <div class="bg-white">
            <h2>User Edit Profile</h2>

            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item">
                    <a href="{{ route('vendor.dashboard') }}">Dashboard</a> 
                </li>
                <li class="breadcrumb-item active">Edit Profile</li>
            </ol>
        </div>
        
        {{ Form::open(['url' => route('vendor.user.update',[$user->id]), 'method' => 'put','files'=>true]) }}

            <div class="row pt-3 bg-white">

                <div class="col-8 text-dark">
            
                    <div class="mb-3">
                        {{ Form::label('name', 'Name') }}
                        {{ Form::text('name',null,['class'=>'form-control','placeholder' => 'Enter Name']) }}
                    </div>

                    <div class="mb-3">
                        {{ Form::label('email','Email') }}
                        {{ Form::email('email',null,['class'=>'form-control','placeholder'=>'Enter Email']) }}
                    </div>

                    <div class="mb-3">
                        {{ Form::label('phone','Phone') }}
                        {{ Form::number('phone',null,['class'=>'form-control','placeholder'=>'Enter Phone No.']) }}
                    </div>

                    <div class="mb-3">
                        {{ Form::label('address','Address') }}
                        {{ Form::textarea('address',null,['class'=>'form-control','placeholder'=>'Enter Address','rows'=>4]) }}
                    </div>

                </div>

                <div class="col-sm-4">
                    <div class="card mb-3">
                        <div class="card-header">Profile Image</div>
                        <div class="card-body text-center">
                            <label class="image-container d-block mb-3">
                                <img src="{{ !empty($user->profile_image) ? url("storage/".$user->profile_image): url('vendor/image/plus.svg') }}" alt="image" id="faviconPreview" class="w-50">
                                <input type="file" name="profile_image" id="favicon" accept="image/" class="d-none file-choose" data-target="#faviconPreview">
                            </label>
                            <label for="favicon" class="btn btn-dark d-block">Upload Pofile</label>
                        </div>
                    </div>
                    
                </div>
            
            </div>

            <div>
                <button type="submit" class="btn btn-primary"> Update </button>
                <button type="reset" class="btn btn-secondary"> Reset </button>
            </div>
        {{ Form::close() }}

    </div>

@endsection