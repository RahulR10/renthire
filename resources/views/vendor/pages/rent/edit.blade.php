@extends('vendor.layout.inner')

@section('site_title','Dashboard')

@section('content')

<div class="container-fluid py-4">
        <div class="bg-white">

            <h2>Edit Rent</h2>

            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item">
                    <a href="{{ route('vendor.dashboard') }}">Dashboard</a> 
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route('vendor.rent.index') }}">Rent</a> 
                </li>
                <li class="breadcrumb-item active">Edit Rent</li>
            </ol>
        </div>
        {!! Form::open
            ([
                'url' => route('vendor.rent.update',
                [$rent ->id]),
                'files'=> true, 
                'method' => 'put'
            ])
        !!}
        @include('backend.pages.rent.form')
        <div>
            <button type="submit" class="btn btn-primary">Update</button>

            <button type="reset" class="btn btn-secondary">Reset</button>
        </div>
        
        {!! Form::close() !!}
    </div>

@endsection