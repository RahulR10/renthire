@extends('vendor.layout.inner')

@section('site_title','Rent')

@section('content')

<div class="container-fluid py-4">
    <div class="bg-white">
     
        <h2>Rent</h2>

        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('vendor.dashboard') }}">Dashboard</a> 
            </li>
            <li class="breadcrumb-item">
                <a href="{{ route('vendor.rent.create') }}">Rent</a> 
            </li>
            <li class="breadcrumb-item active">Add Rent</li>
        </ol>
    </div>

    <!-- <nav class="navbar navbar-light bg-light mb-4">
      <form class="form-inline">
        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
      </form>
    </nav> -->
    @include('backend.template.messages')
    @if($rents->isEmpty())
    <div>

    </div>
    @else
    <div>
      {{ $rents->firstItem() }} to {{ $rents->lastItem() }} from {{ $rents->total() }} are Showing
    </div>
    
    <table class="table table-striped py-5">
      <thead>
        <tr>
          <th>Sr .No</th>
          <th>Vehicle Info.</th>
          <th>Contact Info.</th>
          <th>Vehicle Info.</th>
          <th>Price Type</th>
          <th>Price</th>
          <th>Location</th>
          <th>Image</th>
          <th></th>
        </tr>
      </thead>
    <tbody>
    @foreach($rents as $index => $rent)
    <tr>
      <th>{{ $index + $rents -> firstItem() }}</th>
      <td>
        <div>
          <strong>Type : </strong>{{ $rent -> vehicle_model->make->vehicle_type-> name}}
        </div>
        <div>
          <strong>Model : </strong>{{ $rent -> vehicle_model -> name}}
        </div><div>
          <strong>Make : </strong>{{ $rent -> vehicle_model->make-> name}}
        </div>
      </td>
      <td>
        <div><strong>{{ $rent -> contact_person}}</strong></div>
        <div>{{ $rent -> contact_number}}</div>
      </td>
      <td>
        <div>
          <strong>Year :</strong>{{ $rent -> year}}
        </div>
        <div>
          <strong>Vehicle No. : </strong> {{ $rent->vehicle_number }}
        </div>
        <div>
          <strong>Avg: </strong> {{ $rent->avg_kml }} Kml, {{ $rent->avg_kmh }} Kmh, 
        </div>
        <div>
          <strong>Last KM: </strong> {{ $rent->last_km }}
        </div>
      </td>
      <td>{{ $rent -> price_type}}</td>
      <td>{{ $rent -> price}}</td>
      <td>{{ $rent -> location}}</td>
      <td><img class="img-thumbnail" src="{{ url('image/rents/thumbnail/'. $rent->image) }}" alt=""></td>
      <td>
        <div class="dropdown">
          <button class="btn btn-link" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          More
          </button>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="{{route('vendor.rent.edit',[$rent->id]) }}">Edit</a>
            {{ Form::open(['url' => route('vendor.rent.destroy', [$rent->id]), 'method' => 'DELETE','class' => 'delete-form']) }}
            <button type="button" class="delete-btn dropdown-item">Delete</button>
            {{ Form::close() }}
            @if(!empty($rent->rc))
            <a class="dropdown-item" href="{{ url('storage/'. $rent->rc) }}" target="_blank">Download RC</a>
            @endif
            @if(!empty($rent->insurance))
            <a class="dropdown-item" href="{{ url('storage/'. $rent->insurance) }}" target="_blank">Download Insurance</a>
            @endif
            @if(!empty($rent->tax))
            <a class="dropdown-item" href="{{ url('storage/'. $rent->tax) }}" target="_blank">Download Tax</a>
            @endif
          </div>
        </div>

        <div class="dropdown">
          <button class="btn btn-link" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Daily Progress Report
          </button>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <!-- <a class="dropdown-item" href="{{route('vendor.dpr.create')}}">Add</a> -->
            <a class="dropdown-item" href="{{route('vendor.dpr.index')}}">View</a>
          </div>
        </div>

      </td>
    </tr>
    @endforeach
  </tbody>
</table>
{{ $rents->links("pagination::bootstrap-5") }}
@endif
</div>

@endsection