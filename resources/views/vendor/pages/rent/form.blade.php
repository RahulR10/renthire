@include('vendor.template.messages')
<div class="row pt-5 bg-white">
            <div class="col-8  text-dark">
                <div class="row">
                    <div class="col-6">
                        <div class="mb-3">
                            {{ Form::label('vehicle_type_id', 'Vehicle Type') }}
                            {{Form::select('vehicle_type_id',$vehicle_types,null,
                                [
                                    'class'=>'form-control vtype',
                                    'placeholder' =>'Select Vehicle Type', 
                                    'data-target' => '#make_id', 
                                    'data-apiurl' => route('api.make.index'),
                                    'required'     => 'required'
                                ]
                                )
                            }}
                        </div>
                
                        <div class="mb-3">
                            {{ Form::label('vehicle_model_id', 'Vehicle Model') }}
                            {{Form::select('vehicle_model_id', $vehicle_models, null, [
                                'class'=>'form-control', 
                                'placeholder'=>'Select Vehicle Model',
                                'required'     => 'required'
                                ])
                            }}
                        </div>
                        
                        
                    </div>
                    <div class="col-6">
                        <div class="mb-3">
                            {{ Form::label('make_id', 'Make') }}
                            {{Form::select('make_id', $makes, null, [
                                'class'=>'form-control make', 
                                'placeholder'=>'Select Make', 
                                'data-target' => '#vehicle_model_id', 
                                'data-apiurl' => route('api.model.index'),
                                'required'     => 'required'
                                ])
                            }}
                        </div>
                        <div class="mb-3">
                            {{ Form::label('year','Model Year'),['class' => 'form-label']}}
                            {{ Form::text('year','',[
                                'class'=>'form-control', 
                                'placeholder'=>'Enter Vehicle Model',
                                'required'     => 'required'
                                ])
                            }}
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="mb-3">
                            {{ Form::label('contact_person','Contact Person'),['class' => 'form-label']}}
                            {{ Form::text('contact_person','',[
                                'class'=>'form-control', 
                                'placeholder'=>'Contact Person',
                                'required'     => 'required'
                                ])
                            }}
                        </div>
                        
                    </div>
                    <div class="col-6">
                        <div class="mb-3">
                            {{ Form::label('contact_number','Contact Number'),['class' => 'form-label']}}
                            {{ Form::text('contact_number','',[
                                'class'=>'form-control', 
                                'placeholder'=>'Contact Number',
                                'required'     => 'required'
                                ])
                            }}
                        </div>
                    </div>
                    
                
                    <div class="col-6">
                        <div class="mb-3">
                            {{ Form::label('vehicle_number','Vehicle Number'),['class' => 'form-label']}}
                            {{ Form::text('vehicle_number','',[
                                'class'=>'form-control', 
                                'placeholder'=> 'Enter Vehicle Number',
                                'required'     => 'required'
                                ])
                            }}
                        </div>    
                    </div>

                    <div class="col-6">
                        <div class="mb-3">
                            {{ Form::label('avg_kml','Avg Kml'),['class' => 'form-label']}}
                            {{ Form::text('avg_kml','',[
                                'class'=>'form-control', 
                                'placeholder'=> 'Enter Avg km',
                                'required'     => 'required'
                                ])
                            }}
                        </div>    
                    </div>

                    <div class="col-6">
                        <div class="mb-3">
                            {{ Form::label('avg_kmh','Avg KMh'),['class' => 'form-label']}}
                            {{ Form::text('avg_kmh','',[
                                'class'=>'form-control', 
                                'placeholder'=> 'Enter Avg KMh',
                                'required'     => 'required'
                                ])
                            }}
                        </div>    
                    </div>

                    <div class="col-6">
                    <div class="mb-3">
                            {{ Form::label('last_km','Last Km'),['class' => 'form-label']}}
                            {{ Form::text('last_km','',[
                                'class'=>'form-control', 
                                'placeholder'=> 'Enter Last Km',
                                'required'     => 'required'
                                ])
                            }}
                        </div>    
                    </div>

                    <div class="col-6">
                        <div class="mb-3">
                            {{ Form::label('price_type','Price Type'),['class' => 'form-label']}}
                            {{Form::select('price_type', ['hour' => 'per hour','day' => 'per day','month' => 'per month'], null, ['class'=>'form-control', 'placeholder'=>'Select Price type','required'     => 'required'])}}
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="mb-3">
                            {{ Form::label('price','Price'),['class' => 'form-label']}}
                            {{ Form::text('price','',['class'=>'form-control', 'placeholder'=> 'Enter Price','required'     => 'required'])}}
                        </div>
                    </div>
                    
                    <div class="col-6">
                        <div class="mb-3">
                            {{ Form::label('location','Vehicle Location'),['class' => 'form-label']}}
                            {{ Form::text('location','',[
                                'class'=>'form-control', 
                                'placeholder'=>'Enter Location',
                                'required'     => 'required'
                                ])
                            }}
                        </div>
                    </div>

                </div>
            </div>
            
            <div class="col-sm-4">

                <div class="card mb-3">
                    <div class="card-header">Image</div>
                    <div class="card-body text-center">
                        <label class="image-container d-block mb-3">
                            <img src="{{ !empty($rent->image) ? url("image/rents/thumbnail/".$rent->image): url('admin/image/plus.svg') }}" alt="image" id="imagePreview" class="w-50">
                            <input type="file" name="image" id="image" accept="image/" class="d-none file-choose" data-target="#imagePreview">
                        </label>
                        <label for="image" class="btn btn-dark d-block">Upload Image</label>
                    </div>
                </div>
    
                <div class="card mb-3">
                    <div class="card-header">RC</div>
                    <div class="card-body text-center">
                        <label class="image-container d-block mb-3">
                            <img src="{{ !empty($rent->rc) ? url("storage/".$rent->rc): url('admin/image/plus.svg') }}" alt="rc" id="rcPreview" class="w-50">
                            <input type="file" name="rc" id="rc" accept="image/" class="d-none file-choose" data-target="#rcPreview">
                        </label>
                        <label for="rc" class="btn btn-dark d-block">Upload RC</label>
                    </div>
                </div>

    
                <div class="card mb-3">
                    <div class="card-header">Insurance</div>
                    <div class="card-body text-center">
                        <label class="image-container d-block mb-3">
                            <img src="{{ !empty($rent->insurance) ? url("storage/".$rent->insurance): url('admin/image/plus.svg') }}" alt="insurance" id="insurancePreview" class="w-50">
                            <input type="file" name="insurance" id="insurance" accept="image/" class="d-none file-choose" data-target="#insurancePreview">
                        </label>
                        <label for="insurance" class="btn btn-dark d-block">Upload Insurance</label>
                    </div>
                </div>

                <div class="card mb-3">
                    <div class="card-header">Tax</div>
                    <div class="card-body text-center">
                        <label class="image-container d-block mb-3">
                            <img src="{{ !empty($rent->tax) ? url("storage/".$rent->tax): url('admin/image/plus.svg') }}" alt="tax" id="taxPreview" class="w-50">
                            <input type="file" name="tax" id="tax" accept="image/" class="d-none file-choose" data-target="#taxPreview">
                        </label>
                        <label for="tax" class="btn btn-dark d-block">Upload Tax</label>
                    </div>
                </div>

                {{-- <div>
                    <div class="mb-3">
                        {{Form::label('insurance', 'Upload Insurance'), ['class' => 'form-label']}}
                        {{Form::file('insurance',['class'=>'form-control squareInput'])}}
                    </div>
                </div> --}}
    
                {{-- <div>
                    <div class="mb-3">
                        {{Form::label('tax', 'Upload Tax'), ['class' => 'form-label']}}
                        {{Form::file('tax',['class'=>'form-control squareInput'])}}
                    </div>
                </div> --}}

            </div>
                
        </div>