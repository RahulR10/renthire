<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Vendor - login | Rent & Hire</title>
    <link rel="stylesheet" href="{{ url('frontend/css/style.css') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="frontend/images/Logo.png">
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <!-- Fontawesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>
<body>

    <div class="login-page">
        <div class="login-header">
            <div class="container-fluid">
                <div class="login-title">
                    <a href="">Rent &  Hire - Vendor Registration</a>
                </div>
            </div>
        </div>
        <div class="login-content d-flex align-items-center flex-wrap justify-content-center" style="background : url('http://renthire.in/assets/images/banner.jpg'); background-size: cover">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="login-inner_content bg-white box-shadow border-radius-10">
                            <div class="vendor-login_title">
                                <h2 class="text-center text-primary">Vendor Registration</h2>
                            </div>

                            {{ Form::open([
                                'url'   =>route('web.Vendor.signin.store'),
                                'files' =>true,
                                'method'=>'post',
                                'novalidate' =>true,
                            ]) }}
                                <div class="row">
                                    <div class="col-sm-6">
                                        {{ Form::text('company',null,['class' => 'form-control py-3 mb-3','placeholder' => 'Company Name']) }}

                                        {{ Form::text('name',null,['class' => 'form-control py-3 mb-3','placeholder' => 'Name']) }}
                                    
                                        {{ Form::email('email',null,['class' => 'form-control py-3 mb-3','placeholder' => 'Email']) }}

                                        {{ Form::text('phone',null,['class' => 'form-control py-3 mb-3','placeholder' => 'Contact']) }}
                                        
                                        {{ Form::text('address',null,['class' => 'form-control py-3 mb-3','placeholder' => 'Address']) }}   

                                        {{ Form::text('pan_no',null,['class' => 'form-control py-3 mb-3','placeholder' => 'Pan Card No.']) }}

                                    </div>     

                                    <div class="col-sm-6">
                                        {{ Form::text('company_pan',null,['class' => 'form-control py-3 mb-3','placeholder' => 'Company Pan']) }}

                                        {{ Form::text('company_gstin',null,['class' => 'form-control py-3 mb-3','placeholder' => "Company GSTIN'S" ]) }}
                                    
                                        {{ Form::text('acc_no',null,['class' => 'form-control py-3 mb-3','placeholder' => 'Account No.']) }}

                                        {{ Form::text('acc_name',null,['class' => 'form-control py-3 mb-3','placeholder' => 'Account Holder Name']) }}
                                        
                                        {{ Form::text('ifsc',null,['class' => 'form-control py-3 mb-3','placeholder' => 'ifsc Code']) }}

                                        {{ Form::text('bank_address',null,['class' => 'form-control py-3 mb-3','placeholder' => 'Bank Address']) }}

                                        {{ Form::password('password',null,['class' => 'form-control py-3 mb-3','placeholder' => 'Password']) }}

                                    </div>  

                                </div>
                                
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="input-group justify-content-center py-2 mb-0">
                                            <input type="submit" class="btn btn-primary btn-lg btn-block" value="Submit">
                                        </div>
                                    </div>
                                </div>
                            {{ From::close() }}    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
</body>
</html>