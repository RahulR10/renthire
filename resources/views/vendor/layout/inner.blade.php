@extends('vendor.layout.app')

@section('site_title','Dashboard')

@section('main_content')

@php
    $user = auth()->user();
@endphp

<body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <a class="navbar-brand" href="#">Rent & Hire</a>
            <!-- Navbar-->
            <button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button>
            <ul class="navbar-nav d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                        <a class="dropdown-item" href="{{!empty($user) ? route('vendor.company_edit',$user->id) : '#' }}">Company Profile</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{!empty($user) ? route('vendor.user.edit',$user->id) : '#' }}">Vendor Profile</a>
                        <div class="dropdown-divider"></div>

                        <a class="dropdown-item" href="{{ !empty($user) ? route('vendor.bank_edit',$user->id) : '#' }}">Bank Detail</a>
                        <div class="dropdown-divider"></div>

                        <a class="dropdown-item" href="{{route('vendor.changepassword')}}">Change Password</a>
                        <div class="dropdown-divider"></div>

                        <a class="dropdown-item" href="{{ route('vendor.logout') }}">Logout</a>
                    </div>
                </li>
            </ul>
        </nav>
        <div id="layoutSidenav">
            <x-vendor.nav-sidebar />
            <div id="layoutSidenav_content">

                @yield('content')

                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid">
                        <div class="d-flex align-items-center justify-content-between small">

                            <div class="text-muted">Design and Developed by <a href="https://suncitytechno.com/">Suncity Techno Solution</a></div>
                            
                            <div class="text-muted">Copyright &copy; Your Website 2020</div>
                            
                        </div>
                    </div>
                </footer>
            </div>
        </div>
</body>




@endsection