@extends('frontend.layout.app')

@section('site_title','Rent and Hire')

@section('content')

<section class="slider">
  <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">

    <div class="carousel-inner">
      @foreach ($sliders as $key => $slider )
      <div class="carousel-item @if ($key == '0') active 
        
      @endif ">
        <img src="{{ url("storage/".$slider->image) }}" style="object-fit: cover;height:460px;" class="d-block w-100" alt="slider image">
      </div>
        
      @endforeach
    </div>

    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Next</span>
    </button>
  </div>
</section>

{{-- Start Card --}}

<section class="card py-5">
  <div class="container">

    <div class="section-title">
      <div class="title-h2">For Rent</div>
    </div>

    <div class="row">

      @foreach ($rents as $i => $rent)
        <div class="col-sm-4 py-2">
          <div class="card">
            <img src="{{ url("image/rents/". $rent->image) }}" class="card-img-top" style="height: 180px; object-fit: cover" alt="{{ $rent -> vehicle_model->make-> name}}">
            <div class="card-body">
              <h5 class="card-title">{{ $rent -> vehicle_model -> name}} {{ $rent -> vehicle_model->make-> name}}</h5>
              <p class="card-text py-4">
                <strong>Price</strong> : ₹ {{ $rent -> price}} / {{ $rent -> price_type}} <br><br>
                <strong>Vehicle No.</strong> : {{ $rent->vehicle_number }} <br><br>
                <strong>Avg: </strong> {{ $rent->avg_kml }} Kml, {{ $rent->avg_kmh }} Kmh <br><br>
                <strong>Last KM: </strong> {{ $rent->last_km }} <br><br>
                <strong>Manufacturing Year</strong> : {{ $rent -> year}} <br><br>
                <strong>Vehicle Location</strong> : {{ $rent -> location}}
              </p>
              <a href="{{ route('hire',$rent->id) }}" class="hire-btn">Hire Now</a>
            </div>
          </div>
        </div>
      @endforeach
    </div>

    <div class="text-center py-4">
      <a href="{{ route('rentalProduct') }}" type="submit" class="hire-btn">Show More ...</a>
    </div>
    
  </div>
</section>

{{-- Cart End --}}

{{-- Testimonial start --}}
{{-- 
<section class="testimonials">
  <div class="container py-5">
    <div class="section-title">
      <div class="title-h2">
        Customers Thoughts
      </div>
    </div>
    <div class="wrapper">
      @foreach ($testimonials as $i => $t)
        <div class="col-sm-4">
          <i class="fa-solid fa-quote-left"></i>
          <p>{{ $t->short_description }}</p>
          <div class="content">
            <div class="info">
              <div class="name">{{ $t->name }}</div>
            </div>
          </div>
        </div>
      @endforeach
    </div>
  </div>
</section> --}}

<div class="container mb-5">


  <div class="section-title py-3">
    <div class="title-h2">
      Customers Thoughts
    </div>
  </div>
  
  <div class="row row-cols-1 row-cols-md-3 g-4 py-3">

    @foreach ($testimonials as $i => $t)

    <div class="col">
      <div class="card h-100">
        {{-- <img src="..." class="card-img-top" alt="..."> --}}
        <div class="card-body">
          <h5 class="card-title">{{ $t->name }}</h5>
          <p class="card-text">{{ $t->short_description }}</p>
        </div>
        {{-- <div class="card-footer">
          <small class="text-muted">{{ $t->name }}</small>
        </div> --}}
      </div>
    </div>
    
    @endforeach

  </div>

</div>  

{{-- Testimonial End --}}

{{-- Hire requirement form --}}

<section class="requirement-form">
  <div class="container">
    <div class="form-section">
      <div class="row">
        <div class="col-sm-4">
       
            <img src="frontend/images/contactus-banner.png" alt="" style="width:100%;margin-top:97px">
         
        </div>
        <div class="col-sm-8">
          <div class="header py-3">
            <h3>Hire Requirement</h3>
          </div>
          @include('vendor.template.messages')
          <div class="card mb-5">
            {{ Form::open(['url' => route('admin.hireForm'),'files'=>true,'method'=>'post','novalidate'=>true]) }}
            <div class="row p-5">
              <div class="col-sm-6">

                <div class="mb-4">
  
                  {{ Form::select('make_id',$makes,null,
                    [
                      'class'=>'form-select py-2',
                      'placeholder'=>'Select Vehicle'
                    ]
                    ) 
                  }}
                </div>
                <div class="mb-3">
                  {{ Form::text('name','',['class'=>'form-control py-2','placeholder'=>'Name']) }}
                </div>
                <div class="mb-3">
                  {{ Form::email('mail','',['class'=>'form-control py-2','placeholder'=>'e-Mail']) }}
                </div>
              </div>
              <div class="col-sm-6">
                <div class="mb-3">
                  {{ Form::textarea('message',null,['class'=>'form-control py-4','placeholder' =>'Message','rows'=>'5']) }}
                </div>
              </div>
              <div>
                <button class="btn btn-primary">Send Message</button>
              </div>
            </div>
            {{ Form::close() }}
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

{{-- Hire requirement form End--}}

@endsection