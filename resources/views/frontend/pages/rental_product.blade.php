@extends('frontend.layout.app')

@section('site_title','Rental Products')

@section('content')

<div class="about-header">
    <img src="{{ url('frontend/images/slideshow-1.jpg') }}" alt="">
    <div class="overlay">
        <div class="page-header-title">Rental Product</div>
    </div>
</div>

<section class="card py-5">
    <div class="container">

        <div class="row py-4">
            <div class="col-sm-3">
                <div class="aside">
                    <div class="aside-category">
                        <h4 class="aside-category__title">Brands</h4>
                        <ul class="aside-category__list">
                            <li>
                                <label style="color:#2d2d2d">
                                    <input type="checkbox" class="selector_brand" value="7" style="height:16px;width:16px;">Maruti
                                </label>
                            </li>
                            <li>
                                <label style="color:#2d2d2d">
                                    <input type="checkbox" class="selector_brand" value="7" style="height:16px;width:16px;">Mahendra
                                </label>
                            </li>
                            <li>
                                <label style="color:#2d2d2d">
                                    <input type="checkbox" class="selector_brand" value="7" style="height:16px;width:16px;">Tata
                                </label>
                            </li>
                        </ul>
                    </div>
                    <div class="aside-category">
                        <h4 class="aside-category__title">Category</h4>
                        <ul class="aside-category__list">
                            <li>
                                <label style="color:#2d2d2d">
                                    <input type="checkbox" class="selector_brand" value="7" style="height:16px;width:16px;">TRUCK
                                </label>
                            </li>
                            <li>
                                <label style="color:#2d2d2d">
                                    <input type="checkbox" class="selector_brand" value="7" style="height:16px;width:16px;">Pick Up
                                </label>
                            </li>
                            <li>
                                <label style="color:#2d2d2d">
                                    <input type="checkbox" class="selector_brand" value="7" style="height:16px;width:16px;">Mobile Cranes
                                </label>
                            </li>
                        </ul>
                    </div>
                    <div class="aside-category">
                        <h4 class="aside-category__title">Manufacturing Year</h4>
                        <ul class="aside-category__list">
                            <li>
                                <label style="color:#2d2d2d">
                                    <input type="checkbox" class="selector_brand" value="7" style="height:16px;width:16px;">2000
                                </label>
                            </li>
                            <li>
                                <label style="color:#2d2d2d">
                                    <input type="checkbox" class="selector_brand" value="7" style="height:16px;width:16px;">2005
                                </label>
                            </li>
                            <li>
                                <label style="color:#2d2d2d">
                                    <input type="checkbox" class="selector_brand" value="7" style="height:16px;width:16px;">2010
                                </label>
                            </li>
                        </ul>
                    </div>
                    <div class="aside-category">
                        <h4 class="aside-category__title">Price(Per Hour)</h4>
                        <div class="range-box">
                            <input type="text" class="range-box_input" name="price">
                            <div class="range-box_desc">
                                <span>Range From</span>
                                <input id="from_day" type="text">
                                <span>To</span>
                                <input id="to_day" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="aside-category">
                        <h4 class="aside-category__title">Price(Per Day)</h4>
                        <div class="range-box">
                            <input type="text" class="range-box_input" name="price">
                            <div class="range-box_desc">
                                <span>Range From</span>
                                <input id="from_day" type="text">
                                <span>To</span>
                                <input id="to_day" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="aside-category">
                        <h4 class="aside-category__title">Price(Per Month)</h4>
                        <div class="range-box">
                            <input type="text" class="range-box_input" name="price">
                            <div class="range-box_desc">
                                <span>Range From</span>
                                <input id="from_day" type="text">
                                <span>To</span>
                                <input id="to_day" type="text">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="row">
            
                    @foreach ($rents as $i => $rent)
                        <div class="col-sm-6 mb-2">
                            <div class="card">
                                <img src="{{ url("image/rents/". $rent->image) }}" class="card-img-top" style="height: 180px; object-fit: cover" alt="...">
                                <div class="card-body">
                                <h5 class="card-title">{{ $rent -> vehicle_model -> name}} {{ $rent -> vehicle_model->make-> name}}</h5>
                                <p class="card-text py-4">
                                    <strong>Price</strong> : ₹ {{ $rent -> price}} / {{ $rent -> price_type}} <br><br>
                                    <strong>Vehicle No.</strong> : {{ $rent->vehicle_number }} <br><br>
                                    <strong>Avg: </strong> {{ $rent->avg_kml }} Kml, {{ $rent->avg_kmh }} Kmh <br><br>
                                    <strong>Last KM: </strong> {{ $rent->last_km }} <br><br>
                                    <strong>Manufacturing Year</strong> : {{ $rent -> year}} <br><br>
                                    <strong>Vehicle Location</strong> : {{ $rent -> location }}
                                </p>
                                <a href="{{ route('hire',$rent->id) }}" class="hire-btn">Hire Now</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
        
                </div>
            </div>
        </div>
    </div>
  </section>

@endsection