@extends('frontend.layout.app')

@section('site_title', $page->title)

@section('content')

{{-- <section class="header-about" style="background-image: url(frontend/images/ezgif.com-gif-maker.jpg); height:250px;">
    <div class="about-head-content">
        <div class="about-head-title">About Us</div>
        <div class="about-head-breadcrumb">
            <ul class="breadcrumb">
                <li>
                    <a href="">Home</a>
                </li>
                <li>
                    <span>Our Organisation</span>
                </li>
            </ul>
        </div>
    </div>
</section> --}}

<div class="about-header">
    <img src="{{ url("storage/".$page->image) }}" alt="">
    <div class="overlay">
        <div class="page-header-title">{{ $page->title }}</div>
    </div>
</div>

<section class="about">
    <div class="container">
        <div class="about-para py-4">
            {!! $page->description !!}
        </div>
    </div>
</section>

@endsection