@extends('frontend.layout.app')

@section('site_title','Sucess')

@section('content')

<div class="hire-body">
    <header class="hire-header">
        <h1 class="hire-header__title" data-lead-id="site-header-title">THANK YOU!</h1>
    </header>
    
    <div class="container">
        <div class="row">
            <div class="main-content text-center">
                <i class="fa fa-check main-content__checkmark"></i>
                <div class="col-sm-4 mb-4 m-auto">
                    <div class="equipment-detail mt-2 text-start">
                        <table>
                            <tbody>
                                <tr>
                                    <td>Make</td>
                                    <td>{{ $rent->vehicle_model->make->name }}</td>
                                </tr>
                                <tr>
                                    <td>Manufacturig Year</td>
                                    <td>{{ $rent->year }}</td>
                                </tr>
                                <tr>
                                    <td>Contact Person</td>
                                    <td>{{ $rent->contact_person }}</td>
                                </tr>
                                <tr>
                                    <td>Price Type</td>
                                    <td>Per {{ $hire->price_type }}</td>
                                </tr>
                                <tr>
                                    <td>Price</td>
                                    <td>{{ $hire->price }}</td>
                                </tr>
                                <tr>
                                    <td>Duration</td>
                                    <td>{{ $hire->duration }} {{ $hire->price_type }}</td>
                                </tr>
                                <tr>
                                    <td>Total Price</td>
                                    <td>{{ $hire->total }}</td>
                                </tr>
                                <tr>
                                    <td>Transaction Id</td>
                                    <td>{{ $hire->txn_id }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
    
        </div>
    </div>
</div>

@endsection