@extends('frontend.layout.app')

@section('site_title','Contact')

@section('content')

<div class="about-header">
    <img src="{{ url('frontend/images/slideshow-1.jpg') }}" alt="">
    <div class="overlay">
        <div class="page-header-title">Contact Us</div>
    </div>
</div>

<div class="container py-5">
    <div class="contact-block">
        <div class="contact-grid">
            <div class="first-col">
                <div class="sidebar">
                    <div class="contact-detail">
                        <h4 class="title-contact">Contact Details</h4>
                        <ul class="contact-list">
                            <li class="contact-list-item ">
                                <div class="contact-list-item_icon">
                                    <i class="fa-solid fa-location-dot"></i>
                                </div>
                                <div class="contact-list-item_desc">
                                    <div class="contact-list-item_label">Office Address</div>
                                    <div class="contact-list-item_content">{{ $siteSetting->address }}</div>
                                </div>
                            </li>
                            <div class="border-bootom-width"></div>
                            <li class="contact-list-item">
                                <div class="contact-list-item_icon">
                                    <i class="fa-solid fa-phone-volume"></i>
                                </div>
                                <div class="contact-list-item_desc">
                                    <div class="contact-list-item_label">Contact No.</div>
                                    <div class="contact-list-item_content"><a href="tel:+91{{ $siteSetting->phone }}">+91 {{ $siteSetting->phone }}</a></div>
                                </div>
                            </li>
                            <div class="border-bootom-width"></div>
                            <li class="contact-list-item">
                                <div class="contact-list-item_icon">
                                    <i class="fa-solid fa-location-dot"></i>
                                </div>
                                <div class="contact-list-item_desc">
                                    <div class="contact-list-item_label">send us Mail</div>
                                    <div class="contact-list-item_content"><a href="mailto:{{ $siteSetting->email }}">{{ $siteSetting->email }}</a></div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="second-col">
                <div class="contact-form">
                    <div class="contact-form_title">
                        <h2>Send a Message</h2>
                    </div>
                    <div class="form-content">
                        <form action="" method="post">
                            <div class="row mb-3">
                                <div class="col-6">
                                    {{ Form::text('name',null,['class' => 'form-control py-3','placeholder'=>'Name *']) }}
                                </div>
                                <div class="col-6">
                                    {{ Form::email('email', null,['class' => 'form-control py-3', 'placeholder' => 'Email *']) }}
                                </div>
                            </div>
                            <div class="col-12 mb-3">
                                {{ Form::text('subject',null,['class' => 'form-control py-3', 'placeholder' => 'Subject']) }}
                            </div>

                            <div class="col-12">
                                {{ Form::textarea('message',null,['class' => 'form-control py-3', 'placeholder' => 'Message', 'rows' => '4']) }}
                            </div>

                            <button type="submit" class="submit-form-btn">Send Message <i class="fa-solid fa-arrow-right-to-bracket px-2"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="map">
        <iframe src="{{ $siteSetting->map }}" width="100%" height="400" style="border:1px solid
         #cccc;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
    </div>
</div>

@endsection