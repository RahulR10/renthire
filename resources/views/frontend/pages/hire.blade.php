@extends('frontend.layout.app')

@section('site_title','Hire')

@section('content')

<div class="about-header">
    <img src="{{ url('frontend/images/slideshow-1.jpg') }}" alt="">
    <div class="overlay">
        <div class="page-header-title">Hire</div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-sm-7 py-5">
            <div class="product-img">
                <img src="{{ url("image/rents/". $rent->image) }}" class="card-img-top" alt="{{ $rent->vehicle_model->name }} Image" style="height: 280px; object-fit: cover; border-radius: 12px">
            </div>
        </div>
        <div class="col-sm-5 py-5">
            <div class="hire-title">
                <h2>
                    <span>{{ $rent->vehicle_model->name }}</span>
                    <span>{{ $rent->vehicle_model->make->name }}</span>
                    <span>{{ $rent->vehicle_type }}</span>
                </h2>
            </div>

            <div class="col-sm-12">
                {{ Form::open(['url' =>route('hire.store'),'files'=>true,'method'=>'post','id'=>'checkout-form']) }}
                    <div class="equipment-detail mt-2">
                        <table>
                            <tbody>
                                <tr>
                                    <td>Brand</td>
                                    <td>{{ $rent->vehicle_model->make->name }}</td>
                                </tr>
                                <tr>
                                    <td>Manufacturig Year</td>
                                    <td>{{ $rent->year }}</td>
                                </tr>
                                <tr>
                                    <td>Contact Person</td>
                                    <td>{{ $rent->contact_person }}</td>
                                </tr>
                                <tr>
                                    <td>Vehicle Number</td>
                                    <td>{{ $rent->vehicle_number }}</td>
                                </tr>
                                <tr>
                                    <td>Avg Km/Ltr</td>
                                    <td>{{ $rent->avg_kml }}</td>
                                </tr>
                                <tr>
                                    <td>Avg Km/hr</td>
                                    <td>{{ $rent->avg_kmh }}</td>
                                </tr>
                                <tr>
                                    <td>Last Km</td>
                                    <td>{{ $rent->last_km }}</td>
                                </tr>
                                <tr>
                                    <td>Price Type</td>
                                    <td>{{ $rent->price_type }}</td>
                                </tr>
                                <tr>
                                    <td>Price</td>
                                    <td id="price">{{ $rent->price }}</td>
                                </tr>

                                <input type="hidden" name="rent_id" value="{{ $rent->id }}">
                                <input type="hidden" name="txn_id" id="txnId" value="{{ $rent->txn_id }}">

                                <tr>
                                    <td>No. of {{$rent->price_type }}</td>
                                    <td>
                                        <input type="text" id="num" name="duration" onkeyup="calQty(this.value)" value="1"  min="1" max="100" step="1" for="price"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Total Price</td>
                                    <td id="totalAmount">{{ $rent->price }} </td>
                                </tr>
                                <tr>
                                    <td>Transportation</td>
                                    <td>
                                        <input type="radio" name="user" id="">By User &nbsp;
                                        <input type="radio" name="company" id="">By Company
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-sm-4 mt-4">
                        <div class="hire-submit">
                            <button type="button" class="btn buy_now btn-secondary">Preced To Pay</button>
                        </div>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts') 
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>

<script>

    function calQty(qty) {
        let price = document.getElementById('price').innerText;
        price = parseFloat(price);
        document.getElementById('totalAmount').innerText = price * qty;
    }
    $(function() {
    });

    $(function() {
        $('body').on('click','.buy_now', function(e) {

            var totalAmount = $('#totalAmount').text();

            var options = {
			"key": "rzp_test_7YUA5jcBdwHcnC", // "rzp_test_is9f8HAHz5kblj",
			"amount": (totalAmount * 100), // 2000 paise = INR 20
			"name": "Rent and Hire",
			"description": "Payment",
			"image": "{{ url("storage/".$siteSetting->logo) }}",
			"handler": function(response) {
				$('#txnId').val(response.razorpay_payment_id);
				$('#checkout-form').trigger('submit');
			},
			"prefill": {
				"contact": $('#phone').val(),
				"email": $('#email').val(),
			},
			"theme": {
				"color": "linear-gradient(-145deg, rgba(219,138,222,1) 0%, rgba(246,191,159,1) 100%)"
			}
		};
        var rzp1 = new Razorpay(options);
		rzp1.open();
		e.preventDefault();
        });
    });
</script>
@endpush
