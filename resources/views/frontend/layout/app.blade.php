<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>@yield('site_title') | Heavy Machinary on Rent in India</title>
  <link rel="stylesheet" href="{{ url('frontend/css/style.css') }}">
  {{-- Favicon --}}
  <link rel="icon" type="image/png" sizes="16x16" href="{{ url("storage/".$siteSetting->fav_icon) }}">

  <!-- CSS only -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">

  <!-- Fontawesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />

</head>
<body>
  <div class="page-wrapper">

    <header class="page-header">

      <div class="header-top p-3">
        <div class="container d-flex align-items-center justify-content-between header-col">
          <div class="header-top-left">
            <div class="top-left-login">
              <i class="fa-solid fa-envelope"></i>  <a href="mailto:{{ $siteSetting->email }}">{{ $siteSetting->email }}</a>
            </div>
          </div>
  
          <div class="d-flex header-top-right">
            <div class="login_link px-4 mt-1">
              <i class="fa-solid fa-mobile"></i>
              <a href="{{ $siteSetting->app_link }}">Download App Now</a>
            </div>
            <div class="login_link px-4 mt-1">
              <i class="fa-solid fa-arrow-right-to-bracket"></i>
              <a href="{{ route('vendor.login') }}">Rent Your Vehicle Now!</a>
            </div>
            @if (auth()->check())
            <div class="login_link px-4 mt-1">
              <a class="dropdown-item" href="{{ route('vendor.logout') }}">Logout</a>
            </div>
            @endif 
              
            


          </div>

        </div>

      </div>

      <div id="navbar_top">
        <div class="container px-0">
          <div class="header-bottom">
            <div class="header-bottom-left">
              <div class="logo">
                <a href="{{ route('home') }}" class="logo-link">
                  <img src="{{ url("storage/".$siteSetting->logo) }}" alt="" class="logo-img">
                </a>
              </div>
            </div>
            <div class="header-bottom-right">
              <nav class="navbar navbar-expand-lg">
                <div>
                  <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                  </button>
                  <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                      <li class="nav-item">
                        <a class="nav-link active fs-5 fw-semibold" aria-current="page" href="/">Home</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link active fs-5 fw-semibold" aria-current="page" href="{{ route('page.show', 'about-us') }}">About Us</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link active fs-5 fw-semibold" aria-current="page" href="{{ route('Contact') }}">Contact Us</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link active fs-5 fw-semibold" aria-current="page" href="{{ route('rentalProduct') }}">Rental</a>
                      </li>
                    </ul>
                  </div>
                </div>
              </nav>
            </div>
          </div>
        </div>
      </div>

    </header>

    @yield('content')

    <x-footer />

  </div>

  <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
  <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js" integrity="sha512-aVKKRRi/Q/YV+4mjoKBsE4x3H+BkegoM/em46NNlCqNTmUYADjBbeNefNxYV7giUp0VxICtqdrbqU7iVaeZNXA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script src="src/bootstrap-input-spinner.js"></script>

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>

  <script src="{{ url('frontend/js/app.js') }}"></script>
  
  <script type="text/javascript">

    $(function () {
      ("input[type='number']").inputSpinner();
    });

  </script>
  <script>
    {{ $siteSetting->footer_strip }}
  </script>

  @stack('scripts')

</body>
</html>