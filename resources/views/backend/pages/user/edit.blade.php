@extends('backend.layout.inner')

@section('site_title','Dashboard')

@section('content')

<div class="container-fluid py-4">
    <div class="bg-white">
        <h2>Edit</h2>

        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item">
                <a href="{{ route('admin.dashboard') }}">Dashboard</a> 
            </li>
            <li class="breadcrumb-item">
                <a href="{{ route('admin.user.index') }}">User</a> 
            </li>
            <li class="breadcrumb-item active">Edit User</li>
        </ol>
    </div>
    {!! Form::open
        ([
            'url' => route('admin.user.update',
            [$user->id]),
            'method'=> 'put'
        ]) 
    !!}
    @include('backend.pages.user.form')
    <div>
        <button type="submit" class="btn btn-primary">Update</button>
        
        <button type="reset" class="btn btn-secondary">Reset</button>
    </div>
    {!! Form::close() !!}
</div>

@endsection