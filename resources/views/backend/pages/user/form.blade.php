@include('backend.template.messages')
<div class="row pt-5 bg-white">
            <div class="col-12  text-dark">
                <div class="row">
                    <div class="col-6">
                        <div class="mb-3">
                            {{ Form::label('company','Company')}}
                            {{ Form::text('company',null,['class' => 'form-control', 'placeholder' => ' Company Name','required' => 'required'])}}
                            
                        </div>

                        <div class="mb-3">
                            {{ Form::label('gstin','Company Gstin')}}
                            {{ Form::text('gstin',null,['class' => 'form-control', 'placeholder' => ' Company gstin','required' => 'required'])}}
                            
                        </div>
                    </div>    

                    <div class="col-6">
                        <div class="mb-3">
                            {{ Form::label('pan','Company PAN')}}
                            {{ Form::text('pan',null,['class' => 'form-control', 'placeholder' => ' Company pan','required' => 'required'])}}
                        </div>

                        <div class="mb-3">
                            {{ Form::label('name',' Name')}}
                            {{ Form::text('name',null,['class' => 'form-control', 'placeholder' => '  Name','required' => 'required'])}}
                        </div>
                    
                    </div>    
                    
                    <div class="col-6">

                        <div class="mb-3">
                                {{ Form::label('email','Email')}}
                                {{ Form::email('email',null,['class' => 'form-control', 'placeholder' => 'demo@demo.com','required' => 'required'])}}
                            
                        </div>
    
                        <div class="mb-3">
                                {{ Form::label('phone','Contact Number')}}
                                {{ Form::number('phone',null,['class' => 'form-control', 'placeholder' => '9988XXXXXX','required' => 'required'])}}
                        </div>
                    </div>

                    <div class="col-6">

                        <div class="mb-3">
                            {{ Form::label('address','Address')}}
                            {{ Form::text('address',null,['class' => 'form-control', 'placeholder' => 'address','required' => 'required'])}}
                        </div>
    
                        <div class="mb-3">
                            {{ Form::label('acc_holder','Acc. Holder')}}
                            {{ Form::text('acc_holder',null,['class' => 'form-control', 'placeholder' => 'Acc. Holder Name','required' => 'required'])}}
                        </div>
                    </div>
                    
                    <div class="mb-3">
                        {{ Form::label('acc_no','Acc. No')}}
                        {{ Form::text('acc_no',null,['class' => 'form-control', 'placeholder' => 'Acc. Number','required' => 'required'])}}
                    </div>


                    <div class="mb-3">
                        {{ Form::label('ifsc','Ifsc Code')}}
                        {{ Form::text('ifsc',null,['class' => 'form-control', 'placeholder' => 'ifsc code','required' => 'required'])}}
                        
                    </div>

                    <div class="mb-3">
                        {{ Form::label('bank_address','Bank Address')}}
                        {{ Form::text('bank_address',null,['class' => 'form-control', 'placeholder' => 'bank address','required' => 'required'])}}
                    </div>
                    
                    
                </div>
                

            </div>
        </div>