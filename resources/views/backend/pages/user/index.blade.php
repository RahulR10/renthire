@extends('backend.layout.inner')

@section('site_title','Dashboard')

@section('content')

<div class="container-fluid py-4">
    <div class="bgindex">
      
        <h2>Add Customer</h2>

        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item">
                <a href="{{ route('admin.dashboard') }}">Dashboard</a> 
            </li>
            <li class="breadcrumb-item">
                <a href="{{ route('admin.user.create') }}">Customer</a> 
            </li>
            <li class="breadcrumb-item active">Add Customer</li>
        </ol>
    </div>

    <!-- <nav class="navbar navbar-light bg-light mb-4">
      <form class="form-inline">
        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
      </form>
    </nav> -->
    @include('backend.template.messages')
    @if($users->isEmpty())
    <div>

    </div>
    @else
    <div>
      {{ $users->firstItem() }} to {{ $users->lastItem() }} of {{ $users->total() }} are Showing
    </div>

    <table class="table table-striped py-5">
  <thead>
    <tr>
      <th>Sr .No</th>
      <th>Company</th>
      <th>GSTIN</th>
      <th>PAN</th>
      <th>User Info.</th>
      <th>Bank Details</th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    @foreach($users as $index => $user)
    <tr>
      <th>{{ $index + $users -> firstItem() }}</th>
      <td>{{ $user -> company}}</td>
      <td>{{ $user -> gstin}}</td>
      <td>{{ $user -> pan}}</td>
      <td>
        <div><strong>Name    : </strong>{{ $user -> name}}</div>
        <div><strong>Email   : </strong>{{ $user -> email}}</div>
        <div><strong>Phone   : </strong>{{ $user -> phone}}</div>
        <div><strong>Address :</strong>{{ $user -> address}}</div>
      </td>
      <td>
        <div>
          <strong>Acc. Holder : </strong>{{ $user -> acc_holder}}
        </div>
        <div>
          <strong>Acc. No. : </strong>{{ $user -> acc_no}}
        </div><div>
          <strong>Ifsc : </strong>{{ $user -> ifsc}}
        </div><div>
          <strong>bank Address : </strong>{{ $user -> bank_address}}
        </div>
      </td>
      <td></td>
      <td>
        <div class="dropdown">
          <button class="btn btn-link" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          More
          </button>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="{{route('admin.user.edit',[$user->id]) }}">Edit</a>
            {{ Form::open(['url' => route('admin.user.destroy', [$user->id]), 'method' => 'delete', 'class' =>'delete-form']) }}
            <button type="button" class="delete-btn dropdown-item">Delete</button>
            {{ Form::close() }}
          
          </div>
        </div>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
{{ $users->links("pagination::bootstrap-5") }}
@endif
</div>

@endsection