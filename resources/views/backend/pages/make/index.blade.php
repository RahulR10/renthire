@extends('backend.layout.inner')

@section('site_title','Dashboard')

@section('content')

<div class="container-fluid py-4">
    <div class="bg-white">
        
        <h2>View Make</h2>

        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item">
                <a href="{{ route('admin.dashboard') }}">Dashboard</a> 
            </li>
            <li class="breadcrumb-item">
                <a href="{{ route('admin.make.create') }}">Make</a> 
            </li>
            <li class="breadcrumb-item active">View Make</li>
        </ol>
    </div>

    <!-- <nav class="navbar navbar-light bg-light mb-4">
      <form class="form-inline">
        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
      </form>
    </nav> -->
    @include('backend.template.messages')
    @if($makes->isEmpty())
    <div></div>
    @else
    <div>
      {{ $makes->firstItem() }} to {{ $makes->lastItem() }} from {{ $makes->total() }} are Showing
    </div>

    <table class="table table-striped py-5">
  <thead>
    <tr>
      <th>Sr .No</th>
      <th>Name</th>
      <th>Slug</th>
      <th>Vehicle Type</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    @foreach($makes as $index => $make)
      <tr>
        <th>{{ $index + $makes -> firstItem() }}</th>
        <td>{{ $make -> name}}</td>
        
        <td>{{ $make -> slug}}</td>
        <td>{{ $make -> vehicle_type->name}}</td>
        <td>
          <div class="dropdown">
            <button class="btn btn-link" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            More
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="{{route('admin.make.edit',[$make->id]) }}">Edit</a>
              
              {{ Form::open(['url' => route('admin.make.destroy', [$make->id]), 'method' => 'DELETE', 'class' => 'delete-form']) }}
              <button type="button" class="delete-btn dropdown-item">Delete</button>
              {{ Form::close() }}
            
            </div>
          </div>
        </td>
      </tr>
    @endforeach
  </tbody>
</table>
{{ $makes->links("pagination::bootstrap-5") }}
@endif
</div>

@endsection