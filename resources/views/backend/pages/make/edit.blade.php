@extends('backend.layout.inner')

@section('site_title','Dashboard')

@section('content')

<div class="container-fluid py-4">
        <div class="bg-white">
         
            <h2>Edit Make</h2>

            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.dashboard') }}">Dashboard</a> 
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.make.index') }}">Make</a> 
                </li>
                <li class="breadcrumb-item active">Edit Make</li>
            </ol>

        </div>
        {!! Form::open
            ([
                'url' => route('admin.make.update', 
                [$make-> id]),
                'method' => 'put'
            ]) 
        !!}
        @include('backend.pages.make.form')
        
        <div>

            <button type="submit" class="btn btn-primary">Update</button>

            <button type="reset" class="btn btn-secondary">Reset</button>
        </div>
       
        {!! Form::close() !!}
    </div>

@endsection