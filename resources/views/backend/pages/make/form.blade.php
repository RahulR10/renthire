@include('backend.template.messages')
<div class="row pt-5 bg-white">
    <div class="col-8  text-dark">
        
        <div class="mb-3">
            {!! Form::label('vehicle_type_id', 'vehicle Type') !!}
            {!! Form::select('vehicle_type_id', $vehicle_types,null,['class'=>'form-control','placeholder'=>'Select Vehicle Type','required' => 'required']) !!}
        </div>

        <div class="mb-3">
            {!! Form::label('name' , 'Name')!!}
            {!! Form::text('name', null,['class' => 'form-control title' , 'placeholder' => 'Name','data-target' => '#slug','required' => 'required'])!!}
        </div>
            

        <div class="mb-3">
            {!! Form::label('slug', 'Slug')!!}
            {!! Form::text('slug', null, ['class' => 'form-control' , 'placeholder' => 'Enter Slug','required' => 'required'])!!}
            
        </div>
        
    </div>
</div>