@extends('backend.layout.inner')

@section('site_title','Dashboard')

@section('content')

<div class="container-fluid py-4">
        <div class="bg-white">
            <h2>Add Model</h2>
            
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.dashboard') }}">Dashboard</a> 
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.vehicle_model.index') }}">Vehicle Model</a> 
                </li>
                <li class="breadcrumb-item active">Add Model</li>
            </ol>
        </div>
        {!! Form::open
            ([
                'url' => route('admin.vehicle_model.store'),
                'method' => 'post',
                'novalidate' => true,
            ]) 
        !!}
        @include('backend.pages.vehicle_model.form')
        <div class="pt-3">
                    
            <button type="submit" class="btn btn-primary">Save</button>
        
            <button type="reset" class="btn btn-secondary">Reset</button>
        </div>
        {!! Form::close() !!}
    </div>

@endsection