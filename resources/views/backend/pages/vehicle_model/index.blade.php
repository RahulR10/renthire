@extends('backend.layout.inner')

@section('site_title','Dashboard')

@section('content')

<div class="container-fluid py-4">
    <div class="bgwhite">

        <h2>View Model</h2>

        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item">
                <a href="{{ route('admin.dashboard') }}">Dashboard</a> 
            </li>
            <li class="breadcrumb-item">
                <a href="{{ route('admin.vehicle_model.create') }}">Vehicle Model</a> 
            </li>
            <li class="breadcrumb-item active">View Model</li>
        </ol>
    </div>

    <!-- <nav class="navbar navbar-light bg-light mb-4">
      <form class="form-inline">
        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
      </form>
    </nav> -->
    @include('backend.template.messages')
    @if($vehicle_models->isEmpty())
    <div>
    
    </div>
    @else
    <div>
      {{ $vehicle_models->firstItem() }} to {{ $vehicle_models->lastItem() }} of {{ $vehicle_models->total() }} are showing
    </div>  

    <table class="table table-striped py-5">
      <thead>
        <tr>
          <th>Sr .No</th>
          <th>Name</th>
          <th>Slug</th>
          <th>Vehicle Type</th>
          <th>Make</th>
          <th></th>
        </tr>
      </thead>
  <tbody>
    @foreach($vehicle_models as $index => $vehicle_model)
    <tr>
      <th>{{ $index + $vehicle_models ->firstItem() }}</th>
      <td>{{ $vehicle_model -> name}}</td>
      <td>{{ $vehicle_model -> slug}}</td>
      <td>{{ $vehicle_model -> make->vehicle_type-> name}}</td>
      <td>{{ $vehicle_model -> make-> name}}</td>
      <td>
        <div class="dropdown">
          <button class="btn btn-link" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          More
          </button>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="{{route('admin.vehicle_model.edit',[$vehicle_model->id]) }}">Edit</a>
            {{ Form::open(['url' => route('admin.vehicle_model.destroy', [$vehicle_model->id]), 'method' => 'delete', 'class' => 'delete-form']) }}
            <button type="button" class="delete-btn dropdown-item">Delete</button>
            {{ Form::close() }}
          
          </div>
        </div>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>

{{ $vehicle_models->links("pagination::bootstrap-5") }}
@endif
</div>

@endsection