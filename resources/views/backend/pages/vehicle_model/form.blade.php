@include('backend.template.messages')
<div class="row pt-5 bg-white">
    <div class="col-8  text-dark">
        <div class="row">
            <div class="col-6">
                <div class="mb-3">
                    {{ Form::label('vehicle_type_id', 'Vehicle Type') }}
                    {{Form::select('vehicle_type_id',$vehicle_types,null,
                    ['class'=>'form-control vtype',
                    'placeholder'=>'Select Vehicle Type', 
                    'data-target' => '#make_id', 
                    'data-apiurl' => route('api.make.index'),
                    'required' => 'required'
                    ])}}
                </div>

            </div>
            <div class="col-6">

            <div class="mb-3">
                    {{ Form::label('make_id', 'Make') }}
                    {{Form::select('make_id', $makes, null, 
                    ['class'=>'form-control', 
                    'placeholder'=>'Select Make',
                    'required' => 'required'
                    ])}}
                </div>
            </div>

            
        </div>

        <div class="row">
            <div class="col-6">
                <div class="mb-3">
                    {{ Form::label('name')}}
                    {{ Form::text('name', null, 
                    ['class' => 'form-control title' , 
                    'placeholder' => 'Enter',
                    'data-target' => '#slug',
                    'required' => 'required'
                    ])}}
                </div>
            </div>

            <div class="col-6">

                <div class="mb-3">
                    {!! Form::label('slug') !!}
                    {!! Form::text('slug',null,
                    ['class' => 'form-control' , 
                    'placeholder' => 'Slug',
                    'required' => 'required'
                    ])!!}
                </div>
            </div>
            
        </div>
    </div>
</div>
        