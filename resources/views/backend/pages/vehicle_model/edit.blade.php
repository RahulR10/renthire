@extends('backend.layout.inner')

@section('site_title','Model')

@section('content')

<div class="container-fluid">
    <div class="bgwhite pt-5">

        <h2>Edit Model</h2>

        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item">
                <a href="{{route('admin.dashboard')}}">Dashboard</a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{route('admin.vehicle_model.create')}}">Model</a>
            </li>
            <li class="breadcrumb-item active">Edit Model</li>
        </ol>
    </div>

    {!! Form::open
        ([
            'url' => route('admin.vehicle_model.update',
            [$vehicle_model->id]), 
            'files' => true , 
            'method' => 'put'
        ]) 
    !!}
    @include('backend.pages.vehicle_model.form')

    <div>
        
            <button type="submit" class="btn btn-primary">Update</button>
        
            <button type="reset" class="btn btn-secondary">Reset</button>

    </div>

    {!! Form::close() !!}
</div>

@endsection