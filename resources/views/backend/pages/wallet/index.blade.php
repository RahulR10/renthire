@extends('backend.layout.inner')

@section('site_title','Wallet')

@section('content')
    <div class="container-fluid py-4">
        <div class="bg-white">
            <h2>Wallet</h2>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.dashboard') }}">Dashboard</a>
                </li>
                <li class="breadcrumb-item active">
                    Wallet
                </li>
            </ol>
        </div>
        <div>
            {{ $wallets->firstItem() }} to {{ $wallets->lastItem() }} of {{ $wallets->total() }} are Showing
        </div>
        <table class="table table-striped py-4">
            <thead>
                <tr>
                    <th>Sr .No</th>
                    <th>User Name</th>
                    <th>Payment Type</th>
                    <th>Amount</th>
                    <th>Remarks</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($wallets as $index =>$wallet)
                <tr>    
                    <th>{{ $index + $wallets -> firstItem() }}</th>
                    <td>{{ $wallet -> user->name }}</td>
                    <td>{{ $wallet -> type }}</td>
                    <td>{{ $wallet -> amount }}</td>
                    <td>{{ $wallet -> remark }}</td>
                    <td>
                        @if ($wallet->status == 'Decline')
                            <a href="{{ route('admin.wallet.status',[$wallet->id,'Accept']) }}">
                                {{ $wallet -> status }}
                            </a>
                        @else
                            <a href="{{ route('admin.wallet.status',[$wallet->id,'Decline']) }}">
                                {{ $wallet -> status }}
                            </a>
                        @endif
                    </td>
                </tr>
                
                @endforeach
            </tbody>
        </table>
        {{ $wallets->links("pagination::bootstrap-5") }}
    </div>
@endsection