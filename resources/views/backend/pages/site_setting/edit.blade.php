@extends('backend.layout.inner')

@section('site_title','Site Setting')

@section('content')

    <div class="container-fluid py-4">
        <div class="bg-white">
            <h2>Site Setting</h2>

            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.dashboard') }}">Dashboard</a> 
                </li>
                <li class="breadcrumb-item active">Site Setting</li>
            </ol>
        </div>
        
        {{ Form::open(['url' => route('admin.site_setting.update',[$siteSetting->id]), 'method' => 'put','files'=>true]) }}

            <div class="row pt-3 bg-white">

                <div class="col-8 text-dark">
            
                    <div class="mb-3">
                        {{ Form::label('title', 'Title') }}
                        {{ Form::text('title',null,['class'=>'form-control','placeholder' => 'Enter Title']) }}
                    </div>
            
                    <div class="mb-3">
                        {{ Form::label('tageline','Tageline') }}
                        {{ Form::text('tageline',null,['class'=>'form-control','placeholder' => 'Enter Tageline']) }}
                    </div>

                    <div class="mb-3">
                        {{ Form::label('email','Email') }}
                        {{ Form::email('email',null,['class'=>'form-control','placeholder'=>'Enter Email']) }}
                    </div>

                    <div class="mb-3">
                        {{ Form::label('phone','Phone') }}
                        {{ Form::number('phone',null,['class'=>'form-control','placeholder'=>'Enter Phone No.']) }}
                    </div>

                    <div class="mb-3">
                        {{ Form::label('phone_2','Office No.') }}
                        {{ Form::number('phone_2',null,['class'=>'form-control','placeholder'=>'Enter Office No.']) }}
                    </div>

                    <div class="mb-3">
                        {{ Form::label('admin_margin',null,['class'=>'form-label']) }}
                        {{ Form::number('admin_margin',null,['class'=>'form-control','min'=>'0','placeholder'=>'Admin Margin']) }}
                    </div>

                    <div class="mb-3">
                        {{ Form::label('service_charges',null,['class'=>'form-label']) }}
                        {{ Form::number('service_charges',null,['class'=>'form-control','min'=>'0','placeholder'=>'Service Charges per KM']) }}
                    </div>

                    <div class="mb-3">
                        {{ Form::label('social_links[instagram]', "Instagram URL") }}
                        {{ Form::text('social_links[instagram]', null, ['class' => 'form-control', 'placeholder' => 'Instagram URL']) }}
                    </div>

                    <div class="mb-3">
                        {{ Form::label('social_links[whatsapp]', "What's App URL") }}
                        {{ Form::text('social_links[whatsaap]', null, ['class' => 'form-control', 'placeholder' => "Whats's App URL"]) }}
                    </div>

                    <div class="mb-3">
                        {{ Form::label('social_links[facebook]', "Facebook URL") }}
                        {{ Form::text('social_links[facebook]', null, ['class' => 'form-control', 'placeholder' => 'Facebook URL']) }}
                    </div>

                    <div class="mb-3">
                        {{ Form::label('map','Map') }}
                        {{ Form::textarea('map',null,['class'=>'form-control','placeholder'=>'Enter Map Link','rows'=>3]) }}
                    </div>
                    
                    <div class="mb-3">
                        {{ Form::label('app_link','Application Link') }}
                        {{ Form::text('app_link',null,['class'=>'form-control','placeholder'=>'Enter Application Link']) }}
                    </div>

                    <div class="mb-3">
                        {{ Form::label('footer_script','Footer Script') }}
                        {{ Form::textarea('footer_script',null,['class'=>'form-control','placeholder'=>'Enter Footer Script','rows'=>4]) }}
                    </div>

                    <div class="mb-3">
                        {{ Form::label('address','Address') }}
                        {{ Form::textarea('address',null,['class'=>'form-control','placeholder'=>'Enter Address','rows'=>4]) }}
                    </div>

                </div>

                <div class="col-sm-4">
                    <div class="card mb-3">
                        <div class="card-header">FavIcon</div>
                        <div class="card-body text-center">
                            <label class="image-container d-block mb-3">
                                <img src="{{ !empty($siteSetting->fav_icon) ? url("storage/".$siteSetting->fav_icon): url('admin/image/plus.svg') }}" alt="fav_icon" id="faviconPreview" class="w-50">
                                <input type="file" name="fav_icon" id="favicon" accept="image/" class="d-none file-choose" data-target="#faviconPreview">
                            </label>
                            <label for="favicon" class="btn btn-dark d-block">Upload fav_icon</label>
                        </div>
                    </div>
                    <div class="card mb-2">
                        <div class="card-header">Logo</div>
                        <div class="card-body text-center">
                            <label class="image-container d-block mb-3">
                                <img src="{{ !empty($siteSetting->logo) ? url("storage/".$siteSetting->logo): url('admin/image/plus.svg') }}" alt="logo" id="logoPreview" class="w-50">
                                <input type="file" name="logo" id="logoImage" accept="image/" class="d-none file-choose" data-target="#logoPreview">
                            </label>
                            <label for="logoImage" class="btn btn-dark d-block">Upload Logo</label>
                        </div>
                    </div>
                    <div class="card mb-2">
                        <div class="card-header">Footer Logo</div>
                        <div class="card-body text-center">
                            <label class="image-container d-block mb-3">
                                <img src="{{ !empty($siteSetting->footer_logo) ? url("storage/".$siteSetting->footer_logo): url('admin/image/plus.svg') }}" alt="logo" id="footerLogoPreview" class="w-50">
                                <input type="file" name="footer_logo" accept="image/" class="d-none file-choose" data-target="#footerLogoPreview">
                            </label>
                            <label for="photo" class="btn btn-dark d-block">Upload Logo</label>
                        </div>
                    </div>
                </div>
            
            </div>

            <div>
                <button type="submit" class="btn btn-primary"> Update </button>
                <button type="reset" class="btn btn-secondary"> Reset </button>
            </div>
        {{ Form::close() }}

    </div>

@endsection