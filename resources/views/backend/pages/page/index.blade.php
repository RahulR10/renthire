@extends('backend.layout.inner')

@section('site_title','Page')

@section('content')

<div class="container-fluid py-4">
    <div class="bg-white">
     
        <h2>View Page</h2>

        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('admin.dashboard') }}">Dashboard</a> 
            </li>
            <li class="breadcrumb-item">
                <a href="{{ route('admin.page.create') }}">Page</a> 
            </li>
            <li class="breadcrumb-item active">View Slider</li>
        </ol>
    </div>
    @include('backend.template.messages')
    @if($pages->isEmpty())
    <div>
        Empty
    </div>
    @else
    <div>
      {{ $pages->firstItem() }} to {{ $pages->lastItem() }} from {{ $pages->total() }} are Showing
    </div>
    <table class="table table-striped py-5">
        <thead>
            <tr>
                <th>Sr .No</th>
                <th>Image</th>
                <th>Title</th>
                <th>Description</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($pages as $index => $p)
            <tr>
                <td>{{ $index + $pages -> firstItem() }}</td>
                <td>    
                    <img src="{{ url("storage/".$p -> image) }}" alt="page Us Image"  style="width: 80px;">
                </td>
                <td>{{ $p -> title }}</td>
                <td>{{ $p -> description }}</td>
                <td>
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          More
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                          <a class="dropdown-item" href="{{ route('admin.page.edit',[$p->id]) }}">Edit</a>
                          <a class="dropdown-item" href="#">Delete</a>
                        </div>
                      </div>
                </td>    
            </tr>
            @endforeach
        </tbody>
    </table>
    @endif
</div>

@endsection