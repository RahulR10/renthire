@include('backend.template.messages')

<div class="row pt-3 bg-white">

    <div class="col-8 text-dark">

        <div class="mb-3">
            {{ Form::label('title', 'Title') }}
            {{ Form::text('title',null,['class'=>'form-control','placeholder' => 'Enter Title']) }}
        </div>

        <div class="mb-3">
            {{ Form::label('excerpt', 'Short Description') }}
            {{ Form::textarea('excerpt',null,['class'=>'form-control','id' =>'mytextarea','rows'=>3]) }}
        </div>

        <div class="mb-3">
            {{ Form::label('description', 'Description') }}
            {{ Form::textarea('description',null,['class'=>'form-control','id' =>'mytextarea']) }}
        </div>

        
    </div>
    <div class="col-sm-4">
        <div class="card mb-3">
            <div class="card-header">Image</div>
            <div class="card-body text-center">
                <label class="image-container d-block mb-3">
                    <img src="{{ !empty($page->image) ? url("storage/".$page->image): url('admin/image/plus.svg') }}" alt="image" id="imagePreview" class="w-50">
                    <input type="file" name="image" id="image" accept="image/" class="d-none file-choose" data-target="#imagePreview">
                </label>
                <label for="image" class="btn btn-dark d-block">Upload image</label>
            </div>
        </div>
    </div>

</div>