@extends('backend.layout.inner')

@section('site_title','Admin')

@section('content')

<div class="container-fluid py-4">
    <div class="bg-white">
        
        <h2>Add Staff</h2>

        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item">
                <a href="{{ route('admin.dashboard') }}">Dashboard</a> 
            </li>
            <li class="breadcrumb-item">
                <a href="{{ route('admin.admin.create') }}">Staff</a> 
            </li>
            <li class="breadcrumb-item active">Add Staff</li>
        </ol>

    </div>

    <!-- <nav class="navbar navbar-light bg-light mb-4">
      <form class="form-inline">
        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
      </form>
    </nav> -->

    @include('backend.template.messages')
    @if($admins->isEmpty())
    <div>

    </div>
    @else
    <div>
      {{ $admins->firstItem() }} to {{ $admins->lastItem() }} of {{ $admins->total() }} are Showing
    </div>
    <table class="table table-striped py-5">
  <thead>
    <tr>
      <th>Sr .No</th>
      <th>Name</th>
      <th>Email</th>
      <th>Mobile No.</th>
      <th>Image</th>
      
      <th></th>
    </tr>
  </thead>
  <tbody>
    @foreach($admins as $index => $admin)
    <tr>
      <td>{{ $index + $admins -> firstItem() }}</td>
      <td>{{ $admin -> name}}</td>
      <td>{{ $admin -> email}}</td>
      <td>{{ $admin -> phone}}</td>
      <td>{{ $admin -> image}}</td>
      
      <td>
        <div class="dropdown">
            <button class="btn btn-link" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            More
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="{{route('admin.admin.edit',[$admin->id]) }}">Edit</a>
              {{ Form::open(['url' => route('admin.admin.destroy', [$admin->id]), 'method' => 'DELETE', 'class' => 'delete-form']) }}
              <button Type="button" class="delete-btn dropdown-item">Delete</button>
              {{ Form::close() }}
            
            </div>
          </div>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
{{ $admins->links("pagination::bootstrap-5") }}
@endif
</div>

@endsection