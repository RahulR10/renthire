@extends('backend.layout.inner')

@section('site_title','Admin')

@section('content')

<div class="container-fluid py-4">
        <div class="bg-white">
           
            <h2>Add Staff</h2>

            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.dashboard') }}">Dashboard</a> 
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.admin.index') }}">Staff</a> 
                </li>
                <li class="breadcrumb-item active">Add Staff</li>
            </ol>
            
        </div>
        {!! Form::open
            ([
                'url' => route('admin.admin.store'), 
                'method' => 'post',
                'novalidate'    => true,
            ])
        !!}
            @include('backend.pages.admin.form')
            <div>
                    
                <button type="submit" class="btn btn-primary">Save</button>

                <button type="reset" class="btn btn-secondary">Reset</button>

                
            </div>
        {!! Form::close() !!}
    </div>

@endsection