@extends('backend.layout.inner')

@section('site_title','Admin')

@section('content')

<div class="container-fluid">
    <div class="bg-white">
        <h2>Edit Admin</h2>

        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item">
                <a href="{{route('admin.dashboard')}}">Dashboard</a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{route('admin.admin.index')}}">Admin</a>
            </li>
            <li class="breadcrumb-item">Edit Admin</li>
        </ol>
    </div>
    {!! Form::open
        ([
            'url' => route('admin.admin.store'), 
            'method' => 'post'
        ])
    !!}
            @include('backend.pages.admin.form')
            <div>
                    
                <button type="submit" class="btn btn-primary">Update</button>

                <button type="reset" class="btn btn-secondary">Reset</button>

                
            </div>
    {!! Form::close() !!}
</div>

@endsection