@include('backend.template.messages')
<div class="row pt-5 bg-white">
    <div class="col-8  text-dark">
        
        <div class="mb-3">
            {!! Form::label('name','Name') !!}
            {!! Form::text('name',null,['class' => 'form-control', 'placeholder' => 'Name','required' => 'required'])!!}
        </div>

        <div class="mb-3">
            {!! Form::label('email','Email') !!}
            {!! Form::email('email',null,['class' => 'form-control', 'placeholder' => 'demo@demo.com','required' => 'required'])!!}
        </div>

        <div class="mb-3">
            {!! Form::label('password','Password')!!}
            {!! Form::password('password',null,['class' => 'form-control','placeholder' => 'password','required'=>'required'])!!}
        </div>
        <div class="mb-3">
            {!! Form::label('phone','Mobile No.')!!}
            {!! Form::number('phone',null,['class' => 'form-control','placeholder' => 'Mobile Number','required' => 'required'])!!}
        </div>
        

    </div>

    <div class="col-4">
        <div class="card mb-3">
            <div class="card-header">Image</div>
            <div class="card-body text-center">
                <label class="image-container d-block mb-3">
                    <img src="{{ !empty($admin->image) ? url("storage/".$admin->image): url('admin/image/plus.svg') }}" alt="image" id="imagePreview" class="w-50">
                    <input type="file" name="image" id="image" accept="image/" class="d-none file-choose" data-target="#imagePreview">
                </label>
                <label for="image" class="btn btn-dark d-block">Upload image</label>
            </div>
        </div>
    </div>
</div>
        