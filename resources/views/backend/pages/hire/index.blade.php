@extends('backend.layout.inner')

@section('site_title','Dashboard')

@section('content')

<div class="container py-5">

    <div class="bg-white">
        
        <h2>View Hire</h2>

        <ol class="breadcrumb py-2 mb-4">
          <li class="breadcrumb-item">
            <a href="{{ route('admin.dashboard')}}">Dashboard</a>
          </li>
          <li class="breadcrumb-item">
            <a href="{{ route('admin.hire.create')}}">Hire</a>
          </li>
          <li class="breadcrumb-item active">View Hire</li>
        </ol>

    </div>

    <!-- <nav class="navbar navbar-light bg-light mb-4">
      <form class="form-inline">
        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
      </form>
    </nav> -->

    @if($hires->isEmpty())
    <div>

    </div>
    @else
    <div>
      {{ $hires->firstItem() }} to {{ $hires->lastItem() }} of {{ $hires->total() }} are Showing
    </div>
    <table class="table table-striped py-5">
      <thead>
        <tr>
          <th>Sr .No</th>
          <th>Vehicle Info.</th>
          <th>Rent Type</th>

          <th>Rent</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        @foreach($hires as $index => $hire)
          <tr>
            <th>{{ $index + $hires -> firstItem() }}</th>
            <td>
              <div>
                <strong>Type : </strong>{{ $hire -> rent->vehicle_model->make->vehicle_type-> name}}
              </div>
              <div>
                <strong>Model : </strong>{{ $hire -> rent->vehicle_model -> name}}
              </div><div>
                <strong>Make : </strong>{{ $hire -> rent->vehicle_model->make-> name}}
              </div>
            </td>
            <td>{{ $hire -> rent_type}}</td>
            <td>{{ $hire -> price}}</td>
            <td></td>
            <td>
              <div class="dropdown">
                <button class="btn btn-link" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                More
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                  <a class="dropdown-item" href="{{route('admin.hire.edit',[$hire->id]) }}">Edit</a>
                  {{ Form::open(['url' => route('admin.hire.destroy', [$hire->id]), 'method' => 'delete', 'class' => 'delete-form']) }}
                  <button type="button" class="delete-btn dropdown-item">Delete</button>
                  {{ Form::close() }}
              </div>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>

  {{ $hires->links("pagination::bootstrap-5") }}
  @endif
</div>

@endsection