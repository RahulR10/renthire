<div class="row pt-5 bg-white">
    <div class="col-8  text-dark">
        <div class="row">
            <div class="col-6">
                <div class="mb-3">
                    {{ Form::label('vehicle_type_id', 'Vehicle Type') }}
                    {{Form::select('vehicle_type_id',$vehicle_types,null,[
                        'class'=>'form-control vtype',
                        'placeholder'=>'Select Vehicle Type', 
                        'data-target' => '#make_id', 
                        'data-apiurl' => route('api.make.index'),
                        'required' => 'required'
                        ])
                    }}
                </div>
        
                <div class="mb-3">
                    {{ Form::label('vehicle_model_id', 'Vehicle Model') }}
                    {{Form::select('vehicle_model_id', $vehicle_models, null, [
                        'class'=>'form-control vmodel', 
                        'placeholder'=>'Select Vehicle Model',
                        'required' => 'required',
                        'data-target' => '#rent_id',
                        'data-apiurl' => route('api.rent.index'),
                    ])}}
                </div>
                
                
            </div>
            <div class="col-6">   
                <div class="mb-3">
                        {{ Form::label('make_id', 'Make') }}
                    {{Form::select('make_id', $makes, null, [
                        'class'=>'form-control make', 
                        'placeholder'=>'Select Make', 
                        'data-target' => '#vehicle_model_id', 
                        'data-apiurl' => route('api.model.index'),
                        'required' => 'required'
                        ])
                    }}
                </div>
                
                <div class="mb-3"> 
                    {{ Form::label('user_id', 'User'),['class' => 'form-label']}}
                    {{ Form::select('user_id', $users, null,['class' => 'form-control','placeholder' => 'Rent & Hire','required' => 'required'])}}
                </div>   
                
            </div>
            <div class="col-6">
                <div class="mb-3">
                    {{ Form::label('price_type','Rent Type'),['class' => 'form-label']}}
                    {{ Form::select('price_type',['hour'=>'per hour','day'=>'per day','month'=>'per month'],null,['class'=>'form-control','placeholder'=>'Select Rent Type','required' => 'required'])}}
                </div>   
                
                
            </div>
            <div class="col-6">
                <div class="mb-3">
                    {{ Form::label('price','Rent'),['class' => 'form-label']}}
                    {{ Form::select('rent_id',$rents,null,[
                            'class'=>'form-control rent',
                            'placeholder'=>'Select Rent',
                            'required' => 'required'
                        ])
                    }}
                </div>    
                 
                
                <div class="mb-3">
                    {{ Form::label('duration','Duration'),['class' => 'form-label']}}
                    {{ Form::number('duration',null,[
                            'class'=>'form-control rent',
                            'placeholder'=>'Duration',
                            'required' => 'required'
                        ])
                    }}
                </div>  
                
            </div>
        </div>
        
    </div>
             
            
</div>