@extends('backend.layout.inner')

@section('site_title','Vehicle Type')

@section('content')

<div class="container-fluid py-4">
    <div class="bgwhite">
        
        <h2>View Vehicle Type</h2>

        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item">
                <a href="{{ route('admin.dashboard') }}">Dashboard</a> 
            </li>
            <li class="breadcrumb-item">
                <a href="{{ route('admin.vehicle_type.create') }}">Vehicle Type</a> 
            </li>
            <li class="breadcrumb-item active">View Vehicle Type</li>
        </ol>
    </div>

    <!-- <nav class="navbar navbar-light bg-light mb-4">
      <form class="form-inline">
        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
      </form>
    </nav> -->
    @include('backend.template.messages')
    @if($vehicle_types->isEmpty())
    <div>

    </div>
    @else
    <div>
      {{ $vehicle_types->firstItem() }} to {{ $vehicle_types->lastItem() }} form {{ $vehicle_types->total() }} are Showing
    </div>

    <table class="table table-striped py-5">
  <thead>
    <tr>
      <th>Sr .No</th>
      <th>Name</th>
      <th>Slug</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    @foreach($vehicle_types as $index => $vehicle_type)
    <tr>
      <th>{{ $index + $vehicle_types -> firstItem() }}</th>
      <td>{{ $vehicle_type -> name}}</td>
      
      <td>{{ $vehicle_type -> slug}}</td>

      <td>
        <div class="dropdown">
          <button class="btn btn-link" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          More
          </button>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="{{route('admin.vehicle_type.edit',[$vehicle_type->id]) }}">Edit</a>
            {{ Form::open(['url' => route('admin.vehicle_type.destroy', [$vehicle_type->id]), 'method' => 'DELETE', 'class' => 'delete-form']) }}
            <button type="button" class="delete-btn dropdown-item">Delete</button>
            {{ Form::close() }}
          
          </div>
        </div>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
{{ $vehicle_types->links("pagination::bootstrap-5") }}
@endif
</div>

@endsection