@extends('backend.layout.inner')

@section('site_title','Vehicle Type')

@section('content')

<div class="container-fluid">
    <div class="bgwhite pt-5">

        <h2>Edit vehicle Type</h2>

        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item">
                <a href="{{route('admin.dashboard')}}">Dashboard</a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{route('admin.vehicle_type.create')}}">Vehicle Type</a>
            </li>
            <li class="breadcrumb-item active">Edit Vehicle Type</li>
        </ol>
    </div>

    {!! Form::open
        ([
            'url' => route('admin.vehicle_type.update',
            [$vehicle_type->id]), 
            'method' => 'put'
        ]) 
    !!}
    @include('backend.pages.vehicle_type.form')

    <div>
        
            <button type="submit" class="btn btn-primary">Update</button>

        
        
            <button type="reset" class="btn btn-secondary">Reset</button>

        
    </div>

    {!! Form::close() !!}
</div>

@endsection