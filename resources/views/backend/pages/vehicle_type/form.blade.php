@include('backend.template.messages')
<div class="row pt-5 bg-white">
    <div class="col-8  text-dark">
        
            
        <div class="mb-3">
            {!! Form::label('name') !!}
            {!! Form::text('name',null, ['class' => 'form-control title', 'placeholder' => 'Enter','data-target' => '#slug','required' => 'required']) !!}
        </div>
            
            
        
        <div class="mb-3">
            {!! Form::label('slug', 'SEO Title') !!}
            {!! Form::text('slug', null, ['class' => 'form-control', 'placeholder' => 'Enter Slug','required' =>'required']) !!}
        </div>
        

    </div>
        
        
</div>