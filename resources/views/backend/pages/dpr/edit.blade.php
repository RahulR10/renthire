@extends('backend.layout.inner')

@section('site_title','Daily Progress Report')

@section('content')

<div class="container-fluid py-4">
    <div class="bg-white">

        <h2>Edit Daily Progress Report</h2>

        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item">
                <a href="{{ route('admin.dashboard') }}">Dashboard</a> 
            </li>
            <li class="breadcrumb-item">
                <a href="{{ route('admin.dpr.index') }}">Dpr</a> 
            </li>
            <li class="breadcrumb-item active">Add Dpr</li>
        </ol>

    </div>

    {!! Form::open
        ([
            'url' => route('admin.dpr.update', 
            [$dpr->id]),
            'method' => 'put'
        ])
    !!}
        @include('backend.pages.dpr.form')

        <div>
            <button type="submit" class="btn btn-primary">Update</button>
            
            <button type="reset" class="btn btn-secondary">Reset</button>
        </div>

    {!! Form::close() !!}

</div>

@endsection