@extends('backend.layout.inner')

@section('site_title','Slider')

@section('content')
    <div class="container-fluid py-4">
        <div class="bg-white">
            <h2>Slider</h2>

            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.dashboard') }}">Dashboard</a> 
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.slider.index') }}">Slider</a> 
                </li>
                <li class="breadcrumb-item active">Add Slider</li>
            </ol>
        </div>
        
        {{ Form::open(['url' => route('admin.slider.store'), 'method' => 'post','files'=>true]) }}
            @include('backend.pages.slider.form')
            <div>
                <button type="submit" class="btn btn-primary"> Save </button>
                <button type="reset" class="btn btn-secondary"> Reset </button>
            </div>
        {{ Form::close() }}

    </div>
@endsection