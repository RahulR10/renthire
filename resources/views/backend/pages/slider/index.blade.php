@extends('backend.layout.inner')

@section('site_title','Slider')

@section('content')

<div class="container-fluid py-4">
    <div class="bg-white">
     
        <h2>View Slider</h2>

        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('admin.dashboard') }}">Dashboard</a> 
            </li>
            <li class="breadcrumb-item">
                <a href="{{ route('admin.slider.create') }}">Slider</a> 
            </li>
            <li class="breadcrumb-item active">View Slider</li>
        </ol>
    </div>
    @include('backend.template.messages')
    @if($sliders->isEmpty())
    <div>

    </div>
    @else
    <div>
      {{ $sliders->firstItem() }} to {{ $sliders->lastItem() }} from {{ $sliders->total() }} are Showing
    </div>
    <table class="table table-striped py-5">
        <thead>
            <tr>
                <th>Sr .No</th>
                <th>Title</th>
                <th>Short Description</th>
                <th>Image</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($sliders as $index => $slider)
            <tr>
                <td>{{ $index + $sliders -> firstItem() }}</td>
                <td>{{ $slider -> title }}</td>
                <td>{{ $slider -> excerpt }}</td>
                <td>
                    <img src="{{ url("storage/".$slider -> image) }}" alt="slider Image" style="width: 80px;">
                </td>
                <td>
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          More
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                          <a class="dropdown-item" href="{{ route('admin.slider.edit',[$slider->id]) }}">Edit</a>

                          {{ Form::open([
                            'url' => route('admin.slider.destroy',
                            [$slider->id]),
                            'method' =>'DELETE',
                            'class'=>'delete-form'
                          ]) }}

                          <button type="button" class="delete-btn dropdown-item">Delete</button>
                          {{ Form::close() }}  
                        </div>
                      </div>
                </td>    
            </tr>
            @endforeach
        </tbody>
    </table>
    @endif
</div>

@endsection