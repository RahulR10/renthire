@extends('backend.layout.inner')

@section('site_title','Testimonial')

@section('content')
    <div class="container-fluid py-4">
        <div class="bg-white">
            <h2>Testimonial</h2>

            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.dashboard') }}">Dashboard</a> 
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.testimonial.index') }}">Testimonail</a> 
                </li>
                <li class="breadcrumb-item active">Add Testimonail</li>
            </ol>
        </div>
        
        {{ Form::open(['url' => route('admin.testimonial.store'), 'method' => 'post','files'=>true]) }}
            @include('backend.pages.testimonial.form')
            <div>
                <button type="submit" class="btn btn-primary"> Save </button>
                <button type="reset" class="btn btn-secondary"> Reset </button>
            </div>
        {{ Form::close() }}

    </div>
@endsection