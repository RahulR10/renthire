@include('backend.template.messages')

<div class="row pt-3 bg-white">

    <div class="col-8 text-dark">
        <div class="mb-3">
            {{ Form::label('name','Name') }}
            {{ Form::text('name',null,['class'=>'form-control' ,'placeholder' => 'Name']) }}
        </div>

        <div class="mb-3">
            {{ Form::label('short_description', 'Short Description') }}
            {{ Form::textarea('short_description',null,['class'=>'form-control','placholder' => 'Short Description','rows'=>3]) }}
        </div>

        
    </div>

</div>