@extends('backend.layout.inner')

@section('site_title','Testimonial')

@section('content')

<div class="container-fluid py-4">
    <div class="bg-white">
     
        <h2>View Testimonial</h2>

        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('admin.dashboard') }}">Dashboard</a> 
            </li>
            <li class="breadcrumb-item">
                <a href="{{ route('admin.testimonial.create') }}">Testimonial</a> 
            </li>
            <li class="breadcrumb-item active">View Testimonial</li>
        </ol>
    </div>
    @include('backend.template.messages')
    @if($testimonials->isEmpty())
    <div>

    </div>
    @else
    <div>
      {{ $testimonials->firstItem() }} to {{ $testimonials->lastItem() }} from {{ $testimonials->total() }} are Showing
    </div>
    <table class="table table-striped py-5">
        <thead>
            <tr>
                <th>Sr .No</th>
                <th>Name</th>
                <th>Short Description</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($testimonials as $index => $testimonial)
            <tr>
                <td>{{ $index + $testimonials -> firstItem() }}</td>
                <td>{{ $testimonial -> name }}</td>
                <td>{{ $testimonial -> short_description }}</td>
                <td>
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          More
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                          <a class="dropdown-item" href="{{ route('admin.testimonial.edit',[$testimonial->id]) }}">Edit</a>

                          {{ Form::open([
                            'url' => route('admin.testimonial.destroy',
                            [$testimonial->id]),
                            'method' =>'DELETE',
                            'class'=>'delete-form'
                          ]) }}
                          <button type="button" class="delete-btn dropdown-item">Delete</button>
                          {{ Form::close() }}  
                        </div>
                      </div>
                </td>    
            </tr>
            @endforeach
        </tbody>
    </table>
    @endif
</div>

@endsection