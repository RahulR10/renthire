<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\VehicleType;
use Illuminate\Http\Request;

use App\Http\Requests\VehicleTypeRequest;

class VehicleTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        
        $vehicle_types = VehicleType::paginate(10);
        return view('backend.pages.vehicle_type.index', ['vehicle_types' => $vehicle_types]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.vehicle_type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VehicleTypeRequest $request)
    {
        $vehicle_type = new VehicleType();

        $vehicle_type->name = $request->name;
        $vehicle_type->slug = $request->slug;
        $vehicle_type->save();

        return redirect(route('admin.vehicle_type.index'))->with('success', 'Your data has been Stored in Database.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\vehicle_type  $vehicle_type
     * @return \Illuminate\Http\Response
     */
    public function show(vehicle_type $vehicle_type)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\vehicle_type  $vehicle_type
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, vehicle_type $vehicle_type)
    {
        $request->replace($vehicle_type->toArray());
        $request->flash();

        $data = compact('vehicle_type');
        return view('backend.pages.vehicle_type.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\vehicle_type  $vehicle_type
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, vehicle_type $vehicle_type)
    {
        $vehicle_type->name = $request->name;
        $vehicle_type->slug = $request->slug;
        $vehicle_type->save();

        return redirect(route('admin.vehicle_type.index'))->with('success', 'Your data has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\vehicle_type  $vehicle_type
     * @return \Illuminate\Http\Response
     */
    public function destroy(vehicle_type $vehicle_type)
    {
        if(! empty($vehicle_type->image)){
            $dir = public_path('image/vehicle_types' .$vehicle_type->image);
            if(file_exists($dir))unlink($dir);
            $dir = public_path('images/vehicle_types/thumbnail' .$vehicle_type->image);
            if(file_exists($dir));
        }

        $vehicle_type->delete();
        return redirect(route('admin.vehicle_type.index'))->with('success', 'Your data has been deleted.');
    }
}
