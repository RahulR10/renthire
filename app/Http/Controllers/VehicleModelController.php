<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\VehicleType;
use App\Models\Make;
use App\Models\VehicleModel;
use Illuminate\Http\Request;

use App\Http\Requests\VehicleModelRequest;

class VehicleModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vehicle_types = VehicleType::pluck('name', 'id');
        $makes = [];
        $vehicle_models = VehicleModel::paginate(10);
        return view('backend.pages.vehicle_model.index', compact('vehicle_models', 'vehicle_types', 'makes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $vehicle_types = VehicleType::pluck('name', 'id');
        $makes = [];
        return view('backend.pages.vehicle_model.create', compact('vehicle_types', 'makes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VehicleModelRequest $request)
    {
        $vehicle_model = new VehicleModel();
        $vehicle_model->name = $request->name;
        $vehicle_model->slug = $request->slug;
        $vehicle_model->make_id = $request->make_id;
        $vehicle_model->save();

        return redirect(route('admin.vehicle_model.index'))->with('success', 'You data has been added successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\vehicle_model  $vehicle_model
     * @return \Illuminate\Http\Response
     */
    public function show(vehicle_model $vehicle_model)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\vehicle_model  $vehicle_model
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request ,vehicle_model $vehicle_model)
    {
        $arr = $vehicle_model->toArray();
        $arr['vehicle_type_id'] = $vehicle_model->make->vehicle_type_id;
        $request->replace($arr);
        $request->flash();

        $vehicle_types = VehicleType::pluck('name', 'id');
        $makes = [];
        if(!empty($vehicle_model->make->vehicle_type_id)) {
            $makes = Make::where('vehicle_type_id', $vehicle_model->make->vehicle_type_id)->pluck('name', 'id');
        }

        $makes = Make::where('vehicle_type_id', $vehicle_model->make->vehicle_type_id)->pluck('name', 'id');
        return view('backend.pages.vehicle_model.edit',compact('vehicle_model', 'vehicle_types', 'makes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\vehicle_model  $vehicle_model
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, vehicle_model $vehicle_model)
    {
        $vehicle_model->name = $request->name;
        $vehicle_model->slug = $request->slug;
        $vehicle_model->make_id = $request->make_id;
        $vehicle_model->save();

        return redirect(route('admin.vehicle_model.index'))->with('success','Your data has been updates Succeffully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\vehicle_model  $vehicle_model
     * @return \Illuminate\Http\Response
     */
    public function destroy(vehicle_model $vehicle_model)
    {
        $vehicle_model->delete();

        return redirect(route('admin.vehicle_model.index'))->with('success','Your data has been deleted successfully');
    }
}
