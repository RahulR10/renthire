<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function login(Request $request)
    {
        $request->validate([
            'email'     => 'required|email',
            'password'  => 'required|string|min:8'
        ]);

        $cred = $request->only(['email', 'password']);
        $check = Auth::attempt($cred);
        
        if($check)
        {
            $user = Auth::user();
            $user->api_token = \Str::random(80);
            $user->save();
            return response()->json($user);
        } else {
            return response()->json([
                'message' => 'Login failed! Email or password is not matched.'
            ], 403);
        }

    }

    public function logout(Request $request)
    {
        $request->auth()->logout();
        return response()->json(['message' => 'Successfully logged out']);
    }

    public function forgotPassword(Request $request)
    {
        
    }

    public function changePassword(Request $request)
    {
        $data = $request->all();
        $user = Auth::guard('api')->user();

        if( isset($data['oldPassword'])&& !empty($data['oldPassword'])&& $data['oldPassword'] !== ""&& $data['oldPassword'] !=='undefined')
        {

            
            if($check && isset($data['newPassword']) && !empty($data['newPassword']) && $data['newPassword'] !== "" && $data['newPassword'] !=='undefined')
            {
                $user->password = \Hash::make($data['newPassword']);
                
                $user->save();

                return json_encode(array('token' => $token));
            }
            else
            {
                return "Wrong password information";
            }
        }
        return "Wrong password information";
    }    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'      => 'required|string',
            'email'     => 'required|email|unique:users',
            'phone'     => 'required|string|unique:users',
            'password'  => 'required|string|min:8'
        ]);
        $user = new User();
        $user->name     = $request->name;
        $user->email    = $request->email;
        $user->phone    = $request->phone;
        $user->password = \Hash::make($request->password);
        $user->company  = $request->company;
        $user->api_token = \Str::random(60);
        $user->save();

        return response()->json($user);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        return response()->json(\Auth()->user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
