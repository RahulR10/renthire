<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\VehicleModel;
use Illuminate\Http\Request;

class VehicleModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $models = VehicleModel::where('make_id', $request->makeId)->pluck('name', 'id');

        return response()->json( $models );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\VehicleModel  $vehicle_model
     * @return \Illuminate\Http\Response
     */
    public function show(VehicleModel $vehicle_model)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\VehicleModel  $vehicle_model
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, VehicleModel $vehicle_model)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\VehicleModel  $vehicle_model
     * @return \Illuminate\Http\Response
     */
    public function destroy(VehicleModel $vehicle_model)
    {
        //
    }
}
