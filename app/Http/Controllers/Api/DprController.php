<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\DPR;
use Illuminate\Http\Request;

class DprController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dpr = Dpr::with('rent')->whereHas("rent", function($q){
            return $q->where("user_id",\Auth::user()->id);
        } )->get();

        return response()->json($dpr);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dpr = new Dpr();

        $dpr->user_id = \Auth::user()->id;
        $dpr->rent_id = $request->rent_id;
        $dpr->diesel  = $request->diesel;
        $dpr->km      = $request->km;
        $dpr->hour    = $request->hour;
        $dpr->status  = $request->status;

        $dpr->save();

        return response()->json([
            'message'   => 'Data has been added.',
            'data'      => $dpr
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DPR  $dPR
     * @return \Illuminate\Http\Response
     */
    public function show(DPR $dPR)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DPR  $dPR
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $dPR)
    {
        $dPR = DPR::find($dPR);
        print_r()($dPR->toArray());
        $dPR->status  = $request->status;

        $dPR->save();

        return response()->json([
            'message'   => 'Data has been Updated.',
            'data'      => $dpr
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DPR  $dPR
     * @return \Illuminate\Http\Response
     */
    public function destroy(DPR $dPR)
    {
        //
    }
}
