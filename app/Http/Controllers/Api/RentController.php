<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Rent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class RentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->validate([
            'type'  => 'required'
        ]);
        $rent = Rent::with(['vehicle_model', 'vehicle_model.make', 'vehicle_model.make.vehicle_type']);
        if($request->type === 'rent') {
            $rent->where('user_id',Auth::user()->id)->latest();
        } else {
            $rent->where('user_id', '!=', auth()->user()->id)->orderBy('id');
        }
        // $rent = Rent::where('vehicle_model_id', $request->vehicleModelId)->pluck('name','id');
        // $vehiclemodel = VehicleModel::where('rent_id', $request->rentId) ->pluck('rent type','id'); 
        $rent = $rent->get()->append('name')->append('price_text')->append('make')->append('model')->append('type')->append('files');
        return response()->json( $rent);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rent = new Rent();

        $rent->vehicle_model_id =$request->vehicle_model_id;
        $rent->year =$request->year; 
        $rent->vehicle_number =$request->vehicle_number; 
        $rent->avg_kml =$request->avg_kml; 
        $rent->avg_kmh =$request->avg_kmh; 
        $rent->last_km =$request->last_km;
        $rent->price_type=$request->price_type;
        $rent->contact_person = $request->contact_person;
        $rent->contact_number = $request->contact_number;
        $rent->price = $request ->price;
        $rent->user_id = Auth::user()->id;
        
        if ($request->hasFile('rc')) {
            $path = $request->file('rc')->store('/uploads/rent', 'public');
            $rent->rc = $path;
        }if ($request->hasFile('insurance')) {
            $path = $request->file('insurance')->store('/uploads/rent', 'public');
            $rent->insurance = $path;
        }
        if ($request->hasFile('tax')) {
            $path = $request->file('tax')->store('/uploads/rent', 'public');
            $rent->tax = $path;
        }
        $rent->save();

        if ($request->hasFile('image')) 
        {
            $dir = public_path('image/rents/');
            if (!is_dir($dir) && !file_exists($dir)) {
                mkdir($dir, 0755, true);
            }
            
            $dir2 = $dir . 'thumbnail/';
            if (!is_dir($dir2) && !file_exists($dir2)){
                mkdir($dir2, 0755, true);
            }

            $image       = $request->file('image');
            $filename    = $rent->id . '.' . $image->getClientOriginalExtension(); //$image->getClientOriginalName();
        
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(800, 800 , function($constraint){
                $constraint->aspectRatio();
            });
            $image_resize->save($dir .$filename);

            // Thumbnail
        
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(128, 128, function($constraint){
                $constraint->aspectRatio();
            });
            $image_resize->save($dir2 .$filename);

            $rent->image = $filename;

            $rent->save();
        }

        return response()->json([
            'message'   => 'Data has been added.',
            'data'      => $rent
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Rent  $rent
     * @return \Illuminate\Http\Response
     */
    public function show(Rent $rent)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Rent  $rent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Rent $rent)
    {
        $rent->year =$request->year; 
        $rent->vehicle_number =$request->vehicle_number; 
        $rent->avg_kml =$request->avg_kml; 
        $rent->avg_kmh =$request->avg_kmh; 
        $rent->last_km =$request->last_km;
        $rent->price_type=$request->price_type;
        $rent->contact_person = $request->contact_person;
        $rent->contact_number = $request->contact_number;
        $rent->price = $request ->price;
        $rent->user_id = Auth::user()->id;
        
        if ($request->hasFile('rc')) {
            $path = $request->file('rc')->store('/uploads/rent', 'public');
            $rent->rc = $path;
        }if ($request->hasFile('insurance')) {
            $path = $request->file('insurance')->store('/uploads/rent', 'public');
            $rent->insurance = $path;
        }
        if ($request->hasFile('tax')) {
            $path = $request->file('tax')->store('/uploads/rent', 'public');
            $rent->tax = $path;
        }
        $rent->save();

        if ($request->hasFile('image')) 
        {
            $dir = public_path('image/rents/');
            if (!is_dir($dir) && !file_exists($dir)) {
                mkdir($dir, 0755, true);
            }
            
            $dir2 = $dir . 'thumbnail/';
            if (!is_dir($dir2) && !file_exists($dir2)){
                mkdir($dir2, 0755, true);
            }

            $image       = $request->file('image');
            $filename    = $rent->id . '.' . $image->getClientOriginalExtension(); //$image->getClientOriginalName();
        
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(800, 800 , function($constraint){
                $constraint->aspectRatio();
            });
            $image_resize->save($dir .$filename);

            // Thumbnail
        
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(128, 128, function($constraint){
                $constraint->aspectRatio();
            });
            $image_resize->save($dir2 .$filename);

            $rent->image = $filename;

            $rent->save();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Rent  $rent
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rent $rent)
    {
        //
    }
}
