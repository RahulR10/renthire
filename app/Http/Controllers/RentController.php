<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Rent;
use App\Models\Make;
use App\Models\VehicleType;
use App\Models\VehicleModel;
use Illuminate\Http\Request;

use App\Http\Requests\RentRequest;

use Intervention\Image\Facades\Image;

class RentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vehicle_types = VehicleType::pluck('name', 'id');
        $makes = [];
        
        $rents = rent::paginate(10);
        return view('backend.pages.rent.index', ['rents' => $rents]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $request->flush();

        $vehicle_types = VehicleType::pluck('name', 'id');
        $makes = [];
        $vehicle_models = [];
        return view('backend.pages.rent.create', compact('vehicle_types','makes','vehicle_models'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RentRequest $request)
    {
        $rent = new Rent();

        $rent->vehicle_model_id = $request->vehicle_model_id;
        $rent->year             = $request->year; 
        $rent->vehicle_number   = $request->vehicle_number; 
        $rent->avg_kml          = $request->avg_kml; 
        $rent->avg_kmh          = $request->avg_kmh; 
        $rent->last_km          = $request->last_km;
        $rent->price_type       = $request->price_type;
        $rent->contact_person   = $request->contact_person;
        $rent->contact_number   = $request->contact_number;
        $rent->price            = $request ->price;
        $rent->location            = $request ->location;

        if ($request->hasFile('rc')) {
            $path = $request->file('rc')->store('/uploads/rent', 'public');
            $rent->rc = $path;
        }if ($request->hasFile('insurance')) {
            $path = $request->file('insurance')->store('/uploads/rent', 'public');
            $rent->insurance = $path;
        }
        if ($request->hasFile('tax')) {
            $path = $request->file('tax')->store('/uploads/rent', 'public');
            $rent->tax = $path;
        }
        $rent->save();

        if ($request->hasFile('image')) 
        {
            $dir = public_path('image/rents/');
            if (!is_dir($dir) && !file_exists($dir)) {
                mkdir($dir, 0755, true);
            }
            
            $dir2 = $dir . 'thumbnail/';
            if (!is_dir($dir2) && !file_exists($dir2)){
                mkdir($dir2, 0755, true);
            }

            $image       = $request->file('image');
            $filename    = $rent->id . '.' . $image->getClientOriginalExtension(); //$image->getClientOriginalName();
        
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(800, 800 , function($constraint){
                $constraint->aspectRatio();
            });
            $image_resize->save($dir .$filename);

            // Thumbnail
        
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(128, 128, function($constraint){
                $constraint->aspectRatio();
            });
            $image_resize->save($dir2 .$filename);

            $rent->image = $filename;

            $rent->save();
        }

        return redirect(route('admin.rent.index'))->with('success','Your data has been saved.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Rent  $rent
     * @return \Illuminate\Http\Response
     */
    public function show(Rent $rent)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Rent  $rent
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Rent $rent)
    {
        $arr = $rent ->toArray();
        $arr['vehicle_type_id']= $rent->vehicle_model->make->vehicle_type_id;
        $arr['make_id']= $rent->vehicle_model->make_id;
        $request->replace($arr);
        $request->flash();  

        $vehicle_types = VehicleType::pluck('name', 'id');
        $makes = [];
        $vehicle_models = [];
        if(!empty($rent->vehicle_model->make->vehicle_type_id)) {
            $makes = Make::where('vehicle_type_id', $rent->vehicle_model->make->vehicle_type_id)->pluck('name', 'id');
        }
        if(!empty($rent->vehicle_model->make_id)) {
            $vehicle_models = VehicleModel::where('make_id', $rent->vehicle_model->make_id)->pluck('name', 'id');
        }
        return view('backend.pages.rent.edit', compact('vehicle_types', 'vehicle_models' ,'makes','rent'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Rent  $rent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Rent $rent)
    {
        $rent->vehicle_model_id =$request->vehicle_model_id;
        $rent->year =$request->year; 
        $rent->vehicle_number =$request->vehicle_number; 
        $rent->avg_kml =$request->avg_kml; 
        $rent->avg_kmh =$request->avg_kmh; 
        $rent->last_km =$request->last_km;
        $rent->price_type=$request->price_type;
        $rent->contact_person = $request->contact_person;
        $rent->contact_number = $request->contact_number;
        $rent->price = $request ->price;
        $rent->location            = $request ->location;
        
        if ($request->hasFile('rc')) {
            $path = $request->file('rc')->store('/uploads/rent', 'public');
            $rent->rc = $path;
        }if ($request->hasFile('insurance')) {
            $path = $request->file('insurance')->store('/uploads/rent', 'public');
            $rent->insurance = $path;
        }
        if ($request->hasFile('tax')) {
            $path = $request->file('tax')->store('/uploads/rent', 'public');
            $rent->tax = $path;
        }
        $rent->save();
        
        if ($request->hasFile('image')) 
        {
            $dir = public_path('image/rents/');
            if (!is_dir($dir) && !file_exists($dir)) {
                mkdir($dir, 0755, true);
            }
            
            $dir2 = $dir . 'thumbnail/';
            if (!is_dir($dir2) && !file_exists($dir2)){
                mkdir($dir2, 0755, true);
            }

            $image       = $request->file('image');
            $filename    = $rent->id . '.' . $image->getClientOriginalExtension(); //$image->getClientOriginalName();
        
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(800, 800 , function($constraint){
                $constraint->aspectRatio();
            });
            $image_resize->save($dir .$filename);

            // Thumbnail
        
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(128, 128, function($constraint){
                $constraint->aspectRatio();
            });
            $image_resize->save($dir2 .$filename);

            $rent->image = $filename;

            $rent->save();
        }
        return redirect(route('admin.rent.index'))->with('success', 'Your data has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Rent  $rent
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rent $rent)
    {
        if(!empty($rent->image)){
            $dir = public_path('images/rents' .$rent->image);
            if(file_exists($dir)) unlink($dir);
            $dir = public_path('images/rents/thumbnail' .$rent->image);
            if(file_exists($dir)) unlink($dir);
        }

        $rent->delete();
        return redirect()->back()->with('success','Your data has been deleted.');
    }
}
