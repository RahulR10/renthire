<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function showLogin()
    {
        return view('backend.pages.login');
    }    
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|string|min:8|max:16'
        ]);

        $check = Auth::guard('admin')->attempt( $request->only(['email','password']), $request->remember);

        if($check) {
            return redirect(route('admin.dashboard'));
        }
        else
        {
            return redirect()->back()->withErrors(['msg' => 'Login failed! Email or password is not correct.']);
        }
    }
    
    public function showChangepassword()
    {
        return view('backend.pages.changepassword');
    }

    public function logout()
    {
        Auth::guard('admin')->logout();

        return redirect( route('login') );
    }
}
