<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Slider;
use Illuminate\Http\Request;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders = Slider::paginate(10);
        // dd($sliders);
        return view('backend.pages.slider.index',['sliders' => $sliders]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $slider = new Slider();
        
        $slider->title  = $request->title;
        $slider->excerpt = $request->excerpt;
        // $slider->image  = $request->image;
        if ($request->hasFile('image')) {
            $slider->image = $request->file('image')->store("sliders",['disk' => "public"]);
        }

        $slider->save();

        return redirect(route('admin.slider.index'))->with('Success ! ', 'Your Data has been Submitted Successfully.');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function show(Slider $slider)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,Slider $slider)
    {
        $request->replace($slider->toArray());
        $request->flash();

        $data = compact('slider');
        return view('backend.pages.slider.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Slider $slider)
    {
        $slider->title  = $request->title;
        $slider->excerpt = $request->excerpt;

        if ($request->hasFile('image')) {
            $slider->image = $request->file('image')->store("sliders",['disk' => "public"]);
        }
        $slider->save();

        return redirect(route('admin.slider.index'))->with('Success ! ', 'Your Data has been Updated Successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function destroy(Slider $slider)
    {
        $slider->delete();

        return redirect(route('admin.slider.index'))->with('success','Your Data has been deleted.');
    }
}
