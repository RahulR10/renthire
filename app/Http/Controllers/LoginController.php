<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index()
    {
        return view('backend.pages.login');
    }
    public function login(Request $request)
    {
        $is_login = Auth::guard('admin')->attempt( $request->only(['email','password'],$request->remeber) );

        if ($is_login) {
            return redirect( route('admin.dashboard') );
        } else{
            return redirect()->back()->withErrors(['msg' => 'Login Failed! Credentials are not matched.']);
        }
    }

    public function logout()
    {
        Auth::guard('admin')->logout();

        return redirect(route('admin.login'));
    }

}
