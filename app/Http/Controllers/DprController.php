<?php

namespace App\Http\Controllers;

use App\Models\DPR;
use App\Models\Rent;
use Illuminate\Http\Request;

class DprController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dprs = DPR::paginate(10);
        $rents = Rent::pluck('id');
        return view('backend.pages.dpr.index',compact('rents','dprs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rents = Rent::pluck('id');
        return view('backend.pages.dpr.create', compact('rents'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dpr = new Dpr();

        $dpr->user_id   = $request->user_id;
        $dpr->rent_id   = $request->rent_id;
        $dpr->diesel    = $request->diesel;
        $dpr->kmh       = $request->kmh;
        $dpr->hour      = $request->hour;
        $dpr->status    = $request->status;
        
        $dpr->save();

        return redirect(route('admin.dpr.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\dpr  $dpr
     * @return \Illuminate\Http\Response
     */
    public function show(dpr $dpr)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\dpr  $dpr
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, dpr $dpr)
    {
        $request->replace($dpr->toArray());
        $request->flash();

        $data=compact('dpr');
        return view('backend.pages.dpr.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\dpr  $dpr
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, dpr $dpr)
    {
        $dpr->user_id   = $request->user_id;
        $dpr->rent_id   = $request->rent_id;
        $dpr->diesel    = $request->diesel;
        $dpr->kmh       = $request->kmh;
        $dpr->hour      = $request->hour;
        $dpr->status    = $request->status;

        $dpr->save();

        return redirect(route('admin.dpr.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\dpr  $dpr
     * @return \Illuminate\Http\Response
     */
    public function destroy(dpr $dpr)
    {
        $dpr->delete();
        
        return redirect(route('admin.dpr.index'));
    }
}
