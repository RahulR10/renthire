<?php

namespace App\Http\Controllers\Vendor;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function showLogin()
    {
        return view('vendor.pages.login');
    }    
    public function login(Request $request)
    {
        $redirect_url = session('redirect_url');

        $user = User::where('phone', $request->phone);

        if($user->count()) {
            Auth::login($user->first());
            return redirect( $redirect_url ? $redirect_url : route('vendor.dashboard') );
        } else {
            session(['phone' => $request->phone]);
            return redirect( route('Vendor.signin') );
        }
    }
    
    public function showChangepassword()
    {
        return view('vendor.pages.changepassword');
    }

    public function logout()
    {
        Auth::logout();

        return redirect( route('vendor.login') );
    }

    public function store(UserRequest $request)
    {
        // $user = new User();
        // $user->company      = $request->company;        
        // $user->gstin        = $request->gstin;        
        // $user->pan          = $request->pan;        
        // $user->name         = $request->name;        
        // $user->email        = $request->email;        
        // $user->phone        = session('phone');            
        // $user->address      = $request->address;        
        // $user->acc_holder   = $request->acc_holder;        
        // $user->acc_no       = $request->acc_no;        
        // $user->ifsc         = $request->ifsc;        
        // $user->bank_address = $request->bank_address;   
        // $user->image        = $request->image;
        // dd($user); 
        // $user->save();
        
        // return redirect(route('vendor.user.index'))->with('success','Your data has been submitted successfully');
    }
}
