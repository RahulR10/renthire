<?php

namespace App\Http\Controllers\Vendor;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = user::paginate(10);
        return view('vendor.pages.vendor_sign', ['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $request->flush();
        return view('vendor.pages.vendor_sign');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $user = new User();
        $user->company      = $request->company;        
        $user->gstin        = $request->gstin;        
        $user->pan          = $request->pan;        
        $user->name         = $request->name;        
        $user->email        = $request->email;        
        $user->phone        = $request->phone;        
        $user->password     = Hash::make($request->password); 
        $user->api_token    = \Str::random(60);        
        $user->address      = $request->address;        
        $user->acc_holder   = $request->acc_holder;        
        $user->acc_no       = $request->acc_no;        
        $user->ifsc         = $request->ifsc;        
        $user->bank_address = $request->bank_address;   
        $user->image        = $request->image; 
        $user->save();
        
        return redirect(route('vendor.user.index'))->with('success','Your data has been submitted successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\user  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\user  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, user $user)
    {
        $request->replace($user->toArray());
        $request->flash();

        $data = compact('user');
        return view('vendor.pages.user_setting.edit',$data);
    }

    public function company_edit(Request $request, User $user)
    {
        $request->replace($user->toArray());
        $request->flash();

        $data = compact('user');
        return view('vendor.pages.company_profile.edit',$data);
    }

    public function bank_edit(Request $request, User $user)
    {
        $request->replace($user->toArray());
        $request->flash();

        $data = compact('user');
        return view('vendor.pages.bank_profile.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\user  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, user $user)
    {
        $user->company          = $request->company;        
        $user->gstin            = $request->gstin;        
        $user->pan              = $request->pan;        
        $user->name             = $request->name;        
        $user->email            = $request->email;        
        $user->phone            = $request->phone;         
        $user->address          = $request->address;        
        $user->acc_holder       = $request->acc_holder;        
        $user->acc_no           = $request->acc_no;        
        $user->ifsc             = $request->ifsc;        
        $user->bank_address     = $request->bank_address;   

        if($request->hasFile('profile_image')) {
            $user->profile_image = $request->file('profile_image')->store("user/profile_image",['disk'=>'public']);
        }

        $user->save();
        
        // dd($user);

        return redirect()->back()->with('success','Your data has been updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\user  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(user $user)
    {
        $user->delete();

        return redirect(route('vendor.user.index'))->with('success','Your data has been deleted successfully');
    }
}
