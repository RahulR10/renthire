<?php

namespace App\Http\Controllers\Vendor;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use Illuminate\Http\Request;

use App\Http\Requests\AdminRequest;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admins = admin::paginate(10);

        return view('backend.pages.vendor.index', ['admins' => $admins]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $request->flush();
        return view('backend.pages.vendor.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminRequest $request)
    {
        $admin = new Admin();

        $admin->name       = $request->name;
        $admin->email      = $request->email;
        $admin->password   = $request->password;
        $admin->phone      = $request->phone;
        $admin->image      = $request->image;
        $admin->save();
        
        if($request->hasFile('image'))
        {
            $dir = public_path('image/admins/');
            if(!is_dir($dir) && !file_exists($dir))
            {
                mkdir($dir, 0755, true);
            }

            $dir2 = $dir . 'thumbnail/';
            if(!is_dir($dir2) && !file_exists($dir2))
            {
                mkdir($dir2, 0755, true);
            }

            $image      = $request->file('image');
            $filename   = $admin->id . '.' . $image->getClientOriginalExtension(); //$image->getClientOriginalName();

            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(800, 800, function($constraint){
                $constraint->aspectRatio();
            });
            $image_resize->save($dir .$filename);

            //Thumbnail

            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(128, 128, function($constraint){
                $constraint->aspectRatio();
            });
            $image_resize->save($dir2 .$filename);

            $admin->image = $filename;

            $admin->save();
        }

        return redirect(route('vendor.admin.index'))->with('Success','Your Data has been Saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function show(Admin $admin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Admin $admin)
    {
        $request->replace($admin->toArray());
        $request->flash();

        $data = compact('admin');
        return view('backend.pages.vendor.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Admin $admin)
    {
        $admin->name       = $request->name;
        $admin->email      = $request->email;
        $admin->password   = $request->password;
        $admin->phone      = $request->phone;
        $admin->image      = $request->image;
        $admin->save();

        if($request->hasFile('image'))
        {
            $dir = public_path('image/admins/');
            if(!is_dir($dir) && !file_exists($dir))
            {
                mkdir($dir, 0755, true);
            }

            $dir2 = $dir . 'thumbnail/';
            if(!is_dir($dir2) && !file_exists($dir2))
            {
                mkdir($dir2, 0755, true);
            }

            $image      = $request->file('image');
            $filename   = $admin->id . '.' . $image->getClientOriginalExtension(); //$image->getClientOriginalName();

            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(800, 800, function($constraint){
                $constraint->aspectRatio();
            });
            $image_resize->save($dir .$filename);

            //Thumbnail

            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(128, 128, function($constraint){
                $constraint->aspectRatio();
            });
            $image_resize->save($dir2 .$filename);

            $admin->image = $filename;

            $admin->save();
        }

        
        return redirect(route('vendor.admin.index'))->with('success','Your data has been saved.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function destroy(Admin $admin)
    {

        if(!empty($rent->image)){
            $dir = public_path('images/admin'. $rent->image);
            if(file_exists($dir))unlink($dir);
            $dir = public_path('images/admins/thumbnail'. $rent->image);
            if(file_exists($dir)) unlink($dir);
        }

        $admin->delete();
        return redirect(route('vendor.admin.index'))->with('success','Your data has been deleted.');
    }
}
