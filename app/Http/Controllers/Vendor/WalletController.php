<?php

namespace App\Http\Controllers\vendor;

use App\Http\Controllers\Controller;
use App\Models\Wallet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WalletController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $wallets = Wallet::where('status', 'Accept')->paginate(10);

        $debits = Wallet::where('type','debit')->where('status', 'Accept')->sum('amount');
        $credits = Wallet::where('type','credit')->where('status', 'Accept')->sum('amount');

        $total = $credits-$debits; 

        return view('vendor.pages.wallet.index',compact('wallets','total'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // dd(auth()->user()->id);
        $debits = Wallet::where('type','debit')->where('status', 'Accept')->sum('amount');
        $credits = Wallet::where('type','credit')->where('status', 'Accept')->sum('amount');

        $total = $credits-$debits; 

        return view('vendor.pages.wallet.withdraw',compact('total'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $wallet = new Wallet();

        $wallet->user_id  = Auth::user()->id;
        $wallet->type     = "debit";
        $wallet->amount   = $request->amount;
        $wallet->remark   = $request->remark;
        // $wallet->status   = $request->status;
        // dd($wallet);
        $wallet->save();

        return view('vendor.pages.wallet.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
