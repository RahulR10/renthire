<?php

namespace App\Http\Controllers\Vendor;

use App\Http\Controllers\Controller;
use App\Models\DPR;
use App\Models\Hire;
use App\Models\Rent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DprController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dprs = DPR::paginate(10);
        $rents = Rent::pluck('id');
        return view('vendor.pages.dpr.index',compact('rents','dprs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $hire = Hire::first();
        return view('vendor.pages.dpr.create',compact('hire'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dpr = new Dpr();

        $dpr->user_id   = Auth::user()->id;
        $dpr->hire_id   = $request->hire_id;
        $dpr->diesel    = $request->diesel;
        $dpr->km        = $request->km;
        $dpr->hour      = $request->hour;
        // $dpr->status    = $request->status;
        if ($request->hasFile('signature')) {
            $dpr->signature = $request->file('signature')->store("dpr/signature",['disk'=>'public']);
        }
        $dpr->save();

        return redirect(route('vendor.dpr.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\dpr  $dpr
     * @return \Illuminate\Http\Response
     */
    public function show(dpr $dpr)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\dpr  $dpr
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, dpr $dpr)
    {
        $request->replace($dpr->toArray());
        $request->flash();

        $data=compact('dpr');
        return view('vendor.pages.dpr.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\dpr  $dpr
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, dpr $dpr)
    {
        $dpr->user_id   = $request->user_id;
        $dpr->rent_id   = $request->rent_id;
        $dpr->diesel    = $request->diesel;
        $dpr->km        = $request->km;
        $dpr->hour      = $request->hour;
        $dpr->status    = $request->status;
        
        if ($request->hasFile('signature')) {
            $dpr->signature = $request->file('signature')->store("dpr/signature",['disk'=>'public']);
        }

        $dpr->save();

        return redirect(route('vendor.dpr.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\dpr  $dpr
     * @return \Illuminate\Http\Response
     */
    public function destroy(dpr $dpr)
    {
        $dpr->delete();
        
        return redirect(route('vendor.dpr.index'));
    }
}
