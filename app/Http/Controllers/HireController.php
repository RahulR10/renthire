<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Hire;
use App\Models\User;
use App\Models\Rent;
use App\Models\VehicleType;
use App\Models\VehicleModel;
use App\Models\Make;
use Illuminate\Http\Request;
use App\Http\Requests\HireRequest;

class HireController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vehicle_types = VehicleType::pluck('name','id');
        $makes = [];
        
        $users = Rent::pluck( 'id');
        $hires = Hire::paginate(10);
        return view('backend.pages.hire.index', ['hires' => $hires]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $vehicle_types = VehicleType::get()->pluck('name','id');
        $makes = [];
        $rents = [];
        $vehicle_models = [];
        // $users = Rent::pluck( 'id','name');
        $users = User::get()->pluck('full_name', 'id');
        $rents = Rent::get()->pluck('price','id');
        return view('backend.pages.hire.create',compact('vehicle_types','vehicle_models','rents','users','makes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HireRequest $request)
    {
        // dd($request->rent_id);
        $hire = new Hire();
        $rent = Rent::findOrFail($request->rent_id);
        
        $hire->user_id      = $request->user_id;
        $hire->rent_id      = $request->rent_id;
        $hire->price_type   = $rent->price_type;
        $hire->price        = $rent->price;
        $hire->duration      = $request->duration;
        $hire->total      = $request->duration * $rent->price;

        $hire->save();

        return redirect(route('admin.hire.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Hire  $hire
     * @return \Illuminate\Http\Response
     */
    public function show(Hire $hire)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Hire  $hire
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Hire $hire)
    {

        $arr = $hire ->toArray();
        $arr['vehicle_type_id']= $hire->rent->vehicle_model->make->vehicle_type_id;
        $arr['make_id']= $hire->rent->vehicle_model->make_id;
        

        $request->replace($arr);
        $request->flash();

        $vehicle_types = VehicleType::get()->pluck('name','id');
        $makes = [];
        $vehicle_models = [];
        $users = User::get()->pluck('full_name', 'id');
        $rents = Rent::get()->pluck('price','id');

        if(!empty($hire->rent->vehicle_model->make->vehicle_type_id)) {
            $makes = Make::where('vehicle_type_id', $hire->rent->vehicle_model->make->vehicle_type_id)->pluck('name', 'id');
        }
        if(!empty($hire->rent->vehicle_model->make_id)) {
            $vehicle_models = VehicleModel::where('make_id', $hire->rent->vehicle_model->make_id)->pluck('name', 'id');
        }
       

        $data= compact('hire','vehicle_types','vehicle_models','rents','users','makes');
        return view('backend.pages.hire.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Hire  $hire
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Hire $hire)
    {
        $hire->rent_id = $request->rent_id;
        // $hire->user_id = \Auth::user()->id;
        $hire->price_type =$request->price_type;
        $hire->save();

        return redirect(route('admin.hire.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Hire  $hire
     * @return \Illuminate\Http\Response
     */
    public function destroy(Hire $hire)
    {
        $hire->delete();
        
        return redirect(route('admin.hire.index'));
    }
}
