<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Make;
use App\Models\Rent;
use App\Models\Slider;
use App\Models\Testimonial;
use App\Models\VehicleType;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $sliders = Slider::latest()->get();
        
        $rents   = Rent::latest();
        if(auth()->check()) {
            $rents->where('user_id', '!=', auth()->user()->id);
        }
        $rents = $rents->paginate(4);

        $makes = Make::pluck('name', 'id');

        $testimonials   = Testimonial::latest()->paginate(4);
        return view('frontend.pages.homepage',compact('sliders','rents','testimonials','makes'));
    }
}
