<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Middleware\VendorMiddleware;
use App\Http\Requests\HireRequest;
use App\Models\Hire;
use App\Models\Rent;
use App\Models\SiteSetting;
use App\Models\VehicleType;
use App\Models\Wallet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HireController extends Controller
{
    public function hire(Rent $rent)
    {   
        // $hire = Hire::with('rent')->get();

        return view('frontend.pages.hire',compact('rent'));
    }

    public function store(HireRequest $request,Rent $rent)
    {
        $hire = new Hire();
        $rent = Rent::findOrFail($request->rent_id);

        $hire->user_id      = Auth()->user()->id;
        $hire->rent_id      = $request->rent_id;
        $hire->price_type   = $rent->price_type;
        $hire->price        = $rent->price;
        $hire->duration     = $request->duration;
        $hire->total        = $request->duration * $rent->price;
        $hire->txn_id       = $request->txn_id;

        $hire->save();

        $site_settings = SiteSetting::whereRaw('1=1')->first();
        // dd($site_settings);

        // Wallet insert
        $wallet = new Wallet();
        $wallet->user_id = $rent->user_id;
        $wallet->amount = $hire->total - $hire->total * ($site_settings->admin_margin) / 100;
        $wallet->remark = "Rs.". $wallet->amount ." amount credited for hire your vehicle / machine. " ;
        $wallet->type = "credit";
        $wallet->save();


        return view('frontend.pages.hiredetail',compact('hire','rent'));
    }
}
