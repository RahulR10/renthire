<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class VendorSignController extends Controller
{
    public function vendorsign()
    {
        // dd(session()->all());
        return view('frontend.pages.vendor_sign');
    }

    public function vendorlogin()
    {
        return view('frontend.pages.vendor_login');
    }
    
    public function store(UserRequest $request)
    {
        $user = new User();
        $user->company      = $request->company;        
        $user->gstin        = $request->gstin;        
        $user->pan          = $request->pan;        
        $user->name         = $request->name;        
        $user->email        = $request->email;        
        $user->phone        = session('phone'); // $request->phone;             
        $user->address      = $request->address;        
        $user->acc_holder   = $request->acc_holder;        
        $user->acc_no       = $request->acc_no;        
        $user->ifsc         = $request->ifsc;        
        $user->bank_address = $request->bank_address;    
        $user->save();
        
        dd($user);
        
        session()->forget('phone');

        Auth::login($user);


        return redirect(route('vendor.dashboard'))->with('success','You Have Successfully Registered');

    }
}
