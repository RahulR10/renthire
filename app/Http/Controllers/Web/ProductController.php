<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Models\Rent;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function rentalProduct()
    {
        $rents   = Rent::latest();
        if(auth()->check()) {
            $rents->where('user_id', '!=', auth()->user()->id);
        }
        $rents = $rents->paginate(15);
        return view('frontend.pages.rental_product',compact('rents'));
    }
}
