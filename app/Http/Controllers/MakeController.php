<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\make;
use App\Models\VehicleType;
use Illuminate\Http\Request;

use App\Http\Requests\MakeRequest;

class MakeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $makes = Make::paginate(10);
        return view('backend.pages.make.index', ['makes' => $makes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        
        $vehicle_types= VehicleType::pluck('name','id');
        return view('backend.pages.make.create',compact('vehicle_types'));
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MakeRequest $request)
    {
        $make = new Make();

        $make->name = $request->name;
        $make->slug = $request->slug;
        $make->vehicle_type_id = $request->vehicle_type_id;
        $make->save();

        return redirect(route('admin.make.index'))->with('success','Your data has been saved.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\make  $make
     * @return \Illuminate\Http\Response
     */
    public function show(make $make)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\make  $make
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, make $make)
    {
        $arr['vehicle_type_id'] = $make->vehicle_type_id;
        $request->replace($make->toArray());
        $request->flash();

        $vehicle_types = VehicleType::pluck('name', 'id');
        
        return view('backend.pages.make.edit', compact('make','vehicle_types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\make  $make
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, make $make)
    {
        $make->name = $request->name;
        $make->slug = $request->slug;
        $make->vehicle_type_id = $request->vehicle_type_id;
        $make->save();

        return redirect(route('admin.make.index'))->with('success','Your data has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\make  $make
     * @return \Illuminate\Http\Response
     */
    public function destroy(make $make)
    {
        $make->delete();

        return redirect(route('admin.make.index'))->with('success','Your data has been deleted');
    }
}
