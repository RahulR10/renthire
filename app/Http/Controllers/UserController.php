<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\user;
use Illuminate\Http\Request;

use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = user::paginate(10);
        return view('backend.pages.user.index', ['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $request->flush();
        return view('backend.pages.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $user = new User();
        $user->company      = $request->company;        
        $user->gstin        = $request->gstin;        
        $user->pan          = $request->pan;        
        $user->name         = $request->name;        
        $user->email        = $request->email;        
        $user->phone        = $request->phone;        
        $user->password     = Hash::make($request->password); 
        $user->api_token    = \Str::random(60);       
        $user->address      = $request->address;        
        $user->acc_holder   = $request->acc_holder;        
        $user->acc_no       = $request->acc_no;        
        $user->ifsc         = $request->ifsc;        
        $user->bank_address = $request->bank_address;    
        $user->save();
        
        return redirect(route('admin.user.index'))->with('success','Your data has been submitted successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\user  $user
     * @return \Illuminate\Http\Response
     */
    public function show(user $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\user  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, user $user)
    {
        $request->replace($user->toArray());
        $request->flash();

        $data = compact('user');
        return view('backend.pages.user.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\user  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, user $user)
    {
        $user->company      = $request->company;        
        $user->gstin        = $request->gstin;        
        $user->pan          = $request->pan;        
        $user->name         = $request->name;        
        $user->email        = $request->email;        
        $user->phone        = $request->phone;   
        $user->password     = $request->password;     
        $user->address      = $request->address;        
        $user->acc_holder   = $request->acc_holder;        
        $user->acc_no       = $request->acc_no;        
        $user->ifsc         = $request->ifsc;        
        $user->bank_address = $request->bank_address;    
        $user->save();
        
        return redirect(route('admin.user.index'))->with('success','Your data has been updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\user  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(user $user)
    {
        $user->delete();

        return redirect(route('admin.user.index'))->with('success','Your data has been deleted successfully');
    }
}
