<?php

namespace App\Http\Controllers;

use App\Models\SiteSetting;
use Illuminate\Http\Request;

class SiteSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.site_setting.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SiteSetting  $siteSetting
     * @return \Illuminate\Http\Response
     */
    public function show(SiteSetting $siteSetting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SiteSetting  $siteSetting
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request ,SiteSetting $siteSetting)
    {
        $request->replace($siteSetting->toArray());
        $request->flash();

        $data = compact('siteSetting');
        return view('backend.pages.site_setting.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SiteSetting  $siteSetting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SiteSetting $siteSetting)
    {
        // dd($siteSetting);
        $siteSetting->title          = $request->title;
        $siteSetting->tageline       = $request->tageline;
        $siteSetting->footer_script  = $request->footer_script;
        $siteSetting->email          = $request->email;
        $siteSetting->phone          = $request->phone;
        $siteSetting->phone_2        = $request->phone_2;
        $siteSetting->address        = $request->address;
        $siteSetting->map            = $request->map;
        $siteSetting->social_links   = $request->social_links;
        $siteSetting->admin_margin   = $request->admin_margin;
        $siteSetting->app_link   = $request->app_link;
        $siteSetting->service_charges   = $request->service_charges;

        if ($request->hasFile('logo')) {
            $siteSetting->logo = $request->file('logo')->store("sitesettings/logo",['disk' => "public"]);
        }

        if ($request->hasFile('footer_logo')) {
            $siteSetting->footer_logo = $request->file('footer_logo')->store("sitesettings/footer_logo",['disk' => "public"]);
        }

        if ($request->hasFile('fav_icon')) {
            $siteSetting->fav_icon = $request->file('fav_icon')->store("sitesettings/fav_icon",['disk'=>"public"]);
        }
        
        $siteSetting->save();

        return redirect()->back()->with('success','Your data has been Updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SiteSetting  $siteSetting
     * @return \Illuminate\Http\Response
     */
    public function destroy(SiteSetting $siteSetting)
    {
        //
    }
}
