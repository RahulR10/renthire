<?php

namespace App\Http\Controllers;

use App\Models\Page;
use App\Models\SiteSetting;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $siteSetting = null;

    public function __construct(Request $request)
    {
        $this->siteSetting = $siteSetting = SiteSetting::find(1);
        $page = Page::latest()->get();
        
        \View::share(compact('siteSetting','page'));
    }
}
