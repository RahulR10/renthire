<?php

namespace App\Http\Controllers;

use App\Models\HireForm;
use Illuminate\Http\Request;

class HireFormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $hireForm = new HireForm();
        $hireForm->make_id = $request->make_id;
        $hireForm->name    = $request->name;
        $hireForm->mail    = $request->mail;
        $hireForm->message = $request->message;

        $hireForm->save();

        return redirect()->back()->with('Success','Message sent Successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\HireForm  $hireForm
     * @return \Illuminate\Http\Response
     */
    public function show(HireForm $hireForm)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\HireForm  $hireForm
     * @return \Illuminate\Http\Response
     */
    public function edit(HireForm $hireForm)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\HireForm  $hireForm
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HireForm $hireForm)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\HireForm  $hireForm
     * @return \Illuminate\Http\Response
     */
    public function destroy(HireForm $hireForm)
    {
        //
    }
}
