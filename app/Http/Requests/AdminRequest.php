<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $admin = $this->route('admin');

        return [
            'name'     => 'required',
            'email'    => [
                'required',
                Rule::unique('admins')->using(function($q) use ($admin){
                    
                    if(!empty($admin)){
                        $q->where('id','!=', $admin->id);
                    }
                })
            ],

            'password' => 'required',
            'phone'   => [
                'required',
                Rule::unique('admins')->using(function($q) use ($admin){

                    if(!empty($admin)){
                        $q->where('id','!=', $admin->id);
                    }
                })
            ], 

            'image'    => 'nullable|image|mimes:png,jpg,jpeg,gif,svg|max:2048'
        ];
    }

    public function message()
    {
        return [
            'name.required'     => 'Please enter the Name',
            'email.required'    => 'Please enter the Email',
            'password.required' => 'Please enter the Password',
            'phone.required'   => 'Please enter the Contact number',
        ];
    }
}
