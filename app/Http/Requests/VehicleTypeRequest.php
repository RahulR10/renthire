<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class VehicleTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $vehicle_type = $this->route('vehicle_type');

        return [
            'name'  =>  'required',
            'slug'  =>  [
                'required',
                Rule::unique('vehicle_types')->using(function ($q) use ($vehicle_type){

                    if(!empty($vehicle_type)){
                        $q->where('id', '!', $vehicle_type->id);
                    }
                })
            ],
        ];
    }

    public function message()
    {
        return[
            'name.required' =>  'Please enter Vehicle Type Name',
            'slug.required' =>  'Please enter Vehicle Type slug',
        ];
    }
}
