<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class VehicleModelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $vehicle_model = $this->route('vehicle_model');

        return [
            'vehicle_type_id' => 'required',
            'make_id'         => 'required',
            'name'            => 'required',
            'slug'            => 
                [
                    'required',
                    Rule::unique('vehicle_models','slug')->using(function ($q) use ($vehicle_model){

                        if(!empty($vehicle_model)){

                            $q->where('id','!=',$vehicle_id);

                        }
                    })
                ],           
        ];
    }

    public function message()
    {
        return[
            'vehicle_type_id.required'   =>  'Select the Vehicle type',
            'make_id.required'           =>  'Select the Make',
            'name.required'              =>  'Enter the name',
        ];
    }
}
