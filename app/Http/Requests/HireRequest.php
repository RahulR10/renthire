<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class HireRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $hire = $this->route('hire');

        return [ 
            // 'vehicle_type_id'  => 'required',
            // 'vehicle_model_id' => 'required',
            // 'make_id'          => 'required',
            // 'user_id'          => 'required',
            // 'rent_id'          => 'required|numeric',
            // 'price_type'       => 'required',
            // 'price'            => 'required',  
        ];
    }
}
