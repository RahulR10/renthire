<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = $this->route('user');

        return [
            'company'   =>  'required',
            'name'      =>  'required',
            'email'     =>  [
                'required',
                Rule::unique('users','email')->using(function ($q) use ($user){

                    if(!empty($user)) {

                        $q->where('id','!=',$user->id);
                    }
                })
            ],
            // 'password'  =>  'required',
            'address'   =>  'required',
            'acc_holder'=>  'required',
            'acc_no'    =>  [
                'required',
                Rule::unique('users','acc_no')->using(function($q) use ($user){

                    if(!empty($user)){
                        $q->where('id','!=', $user->id);
                    }
                })
            ],
            'ifsc'  =>  'required',
            'bank_address'  =>  'required',
        ];
    }

    public function message()
    {
        return[
            'company.required'      =>  'Enter the company Name',
            'name.required'         =>  'Enter the Name',
            'email.required'        =>  'Enter the email',
            // 'phone.required'        =>  'Enter the phone',
            // 'password.requored'     =>  'Enter the password',
            'address.required'      =>  'Enter the address',
            'acc_holder.required'   =>  'Enter the acc holder name',
            'acc_no.required'       =>  'Enter the acc number',
            'ifsc.required'         =>  'Enter the ifsc code',
            'bank_address.required' =>  'Enter the bank address',
        ];
    }
}
