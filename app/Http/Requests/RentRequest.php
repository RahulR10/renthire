<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rent = $this->route('rent');
        
        return [
            'vehicle_type_id' => 'required',
            'vehicle_model_id' => 'required',
            'make_id' => 'required',
            'year' => 'required',
            'contact_person' => 'required',
            'contact_number' => 'required',
            'vehicle_number' => 'required',
            'avg_kml' => 'required',
            'avg_kmh' => 'required',
            'last_km' => 'required',
            'price' => 'required',

            'image' => 'nullable|image|mimes:png,jpg,jpeg,gif,svg|max:2048'
        ];
    }

    public function messages()
    {
        return[
            'vehicle_type_id.required'   => 'Select Vehicle Type',
            'vehicle_model_id.required'  => 'Select Vehicle Model',
            'make_id.required'           => 'Select Make',
            'year.required'              => 'Enter Vehicle year',
            'contact_person.required'    => 'Enter Contact Person Name',
            'contact_number.required'    => 'Enter Contact Number of Person',
            'vehicle_number.required'    => 'Enter Vehicle Number',
            'avg_kml.required'           => 'Enter Avg Kml',
            'avg_kmh.required'           => 'Enter Avg Kmh',
            'last_km.required'           => 'Enter Last Avg',
            'price.required'             => 'Enter price',
        ];
    }
}
