<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class MakeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $make = $this->route('make');

        return [
            'vehicle_type_id'   => 'required',
            'name'              => 'required',
            'slug'              => 'required',
        ];
    }

    public function message()
    {
        return[
            'vehicle_type_id.required'  =>  'Select the type',
            'name.required'             =>  'Enter the name',
            'slug.required'             =>  'Enter the slug',
        ];
    }
}
