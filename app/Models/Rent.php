<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rent extends Model
{
    use HasFactory;

    protected $appends = ['name', 'files', 'price_text', 'make', 'model', 'type'];

    public function getNameAttribute()
    {
        $model = $this->vehicle_model;
        $make  = $model->make;
        $vtype = $make->vehicle_type->name;
        $id    = sprintf('#%04d', $this->id); // sprintf = give output
        return "{$make->name} {$model->name} - {$vtype} ({$id})";
    }

    public function getFilesAttribute()
    {
        $image = !empty($this->image) ? url('image/rents/thumbnail/' . $this->image) : "";
        $full_image = !empty($this->image) ? url('image/rents/' . $this->image) : "";
        $rc = !empty($this->rc) ? url('storage/' . $this->rc) : "";
        $insurance = !empty($this->insurance) ? url('storage/' . $this->insurance) : "";
        $tax = !empty($this->tax) ? url('storage/' . $this->tax) : "";
        return compact( 'full_image', 'image', 'rc', 'insurance', 'tax');
    }

    public function getPriceTextAttribute()
    {
        return "₹{$this->price} per {$this->price_type}";
    }

    public function getMakeAttribute()
    {
        $model = $this->vehicle_model;
        $make  = $model->make;
        return $make->name ?: '';
    }

    public function getModelAttribute()
    {
        $model = $this->vehicle_model;

        return $model->name;
    }

    public function getTypeAttribute()
    {
        $model = $this->vehicle_model;
        $make  = $model->make;
        $vtype = $make->vehicle_type->name;
        return $vtype;
    }

    public function vehicle_model()
    {
        return $this->belongsTo(VehicleModel::class);
    }

}
