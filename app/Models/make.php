<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Make extends Model
{
    use HasFactory;

    public function vehicle_type()
    {
        return $this->belongsTo(VehicleType::class);
    }

    public function vehicle_model()
    {
        return $this->hasMany(VehicleModel::class);
    }
}
