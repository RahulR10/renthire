<?php

namespace Database\Seeders;

use App\Models\Page;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Page::insert([
            [
                'title' => 'About Us',
                'slug' => 'about-us',
            ],
            [
                'title' => 'Terms & Conditions',
                'slug' => 'terms-and-conditions',
            ],
            [
                'title' => 'Privacy Policy',
                'slug' => 'privacy-policy',
            ],
        ]);
    }
}
