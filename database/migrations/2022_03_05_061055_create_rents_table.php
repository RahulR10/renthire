<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rents', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('vehicle_model_id');
            $table->foreign('vehicle_model_id')->references('id')->on('vehicle_models')->onDelete('cascade');
            $table->year('year')->nullable();
            $table->string('contact_person');
            $table->string('contact_number');
            $table->string('vehicle_number');
            $table->double('avg_kml')->nullable();
            $table->double('avg_kmh')->nullable();
            $table->double('last_km')->nullable();
            $table->enum('price_type',['hour','month','day'])->default('day');
            $table->double('price')->nullable();
            $table->string('image',191)->nullable();
            $table->string('rc',191)->nullable();
            $table->string('insurance',191)->nullable();
            $table->string('tax',191)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rents');
    }
};
