<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('d_p_r_s', function (Blueprint $table) {
            // $table->dropForeign(['rent_id']);
            // $table->dropColumn(['rent_id']);

            $table->foreignId('hire_id')->after('user_id')->constrained()->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('d_p_r_s', function (Blueprint $table) {
            //
        });
    }
};
