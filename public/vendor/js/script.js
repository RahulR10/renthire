(function($) {
    "use strict";

    // Add active state to sidbar nav links
    var path = window.location.href; // because the 'href' property of the DOM element is the absolute path
        $("#layoutSidenav_nav .sb-sidenav a.nav-link").each(function() {
            if (this.href === path) {
                $(this).addClass("active");
            }
        });

    // Toggle the side navigation
    $("#sidebarToggle").on("click", function(e) {
        e.preventDefault();
        $("body").toggleClass("sb-sidenav-toggled");
    });
})(jQuery);

$(function() {
    $(document).on('change', '.vtype', function (e) {
        var sel = $(this).data('target');
        $.ajax({
            url: $(this).data('apiurl'),
            method: 'GET',
            data: {
                vehicleTypeId: $(this).val()
            },
            success: function (result) {
                $(sel).html(
                    `<option value>Select Type</option>`
                );
                
                for(let id in result) {
                    $(sel).append(
                        `<option value="${id}">${result[id]}</option>`
                    );
                }
            }
        });
    });

    $(document).on('change', '.make', function (e) {
        var sel = $(this).data('target');
        $.ajax({
            url: $(this).data('apiurl'),
            method: 'GET',
            data: {
                makeId: $(this).val()
            },
            success: function (result) {
                $(sel).html(
                    `<option value>Select Vehicle Model</option>`
                );
                
                for(let id in result) {
                    $(sel).append(
                        `<option value="${id}">${result[id]}</option>`
                    );
                }
            }
        });
    });
});

function convertToSlug(Text) {
    return Text.toLowerCase()
      .replace(/ /g, '-')
      .replace(/[^\w-]+/g, '');
}
  
$(function () {
    $(document).on('keyup', '.title', function () {
      var sel = $(this).data('target');
  
      $(sel).val(convertToSlug($(this).val()));
    });      
});

$(function(){
    $(document).on('click','.delete-form .delete-btn',function(){
        var form = $(this).closest('.delete-form');

        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
          }).then((result) => {
            if(result.isConfirmed){
                form.trigger('submit');
            }
          })
    })

    $('.file-choose').on("change",function(){
        const file = this.files[0];
        let target = $(this).data('target');
        console.log( target);
        if (file){
          let reader = new FileReader();
          reader.onload = function(event){
            console.log(event.target.result);
            $(target).attr('src', event.target.result);
          }
          reader.readAsDataURL(file);
        }
      });
});